use utils::types::*;

const ORDER: [(usize, usize); 100] = {
    let mut ordering = [(0, 0); 100];
    let mut y = 0;
    while y < 10 {
        let mut x = 0;
        while x < 10 {
            ordering[10 * y + x] = (x, y);
            x += 1;
        }
        y += 1;
    }
    ordering
};

fn step(grid: &mut Vec<Vec<u8>>) -> bool {
    let mut to_bump = ORDER.to_vec();
    let mut has_flashed = [[false; 10]; 10];
    let mut flashed = 0;
    while let Some((x, y)) = to_bump.pop() {
        if has_flashed[x][y] {
            continue;
        }
        grid[x][y] += 1;
        if grid[x][y] > 9 {
            // This one is flashing
            flashed += 1;
            has_flashed[x][y] = true;
            grid[x][y] = 0;
            for coords in utils::around(x, y, 10, true) {
                to_bump.push(coords);
            }
        }
    }
    flashed == 100
}

pub fn day(contents: String) -> Return {
    let len = contents
        .find('\n')
        .ok_or_else(|| Error::Parsing(contents.to_string()))?;
    let mut grid: Vec<Vec<u8>> = vec![vec![0; len]; len];
    for (y, line) in contents.lines().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            grid[x][y] = ch.to_string().parse::<u8>()?;
        }
    }

    let mut i = 1;
    loop {
        if step(&mut grid) {
            return ret(i);
        }
        i += 1;
    }
}

#[cfg(test)]
mod day11 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 195);
        Ok(())
    }
}
