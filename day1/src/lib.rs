use utils::types::*;

pub fn day(contents: String) -> Return {
    let windowed_sums: Vec<usize> = contents
        .lines()
        .map(|line| Ok(line.parse()?))
        .collect::<Result<Vec<usize>>>()?
        .windows(3)
        .map(|win| win.iter().sum())
        .collect();
    let mut count = 0;
    for i in 1..windowed_sums.len() {
        if windowed_sums[i - 1] < windowed_sums[i] {
            count += 1;
        }
    }
    ret(count)
}

#[cfg(test)]
mod day1 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 5);
        Ok(())
    }
}
