#!/bin/fish

# Find the current number
set curr_num (rg "NB_DAYS: usize = (\d+)" aoc2020/src/main.rs -Nor '$1')
set new_num (math $curr_num + 1)

if test $new_num -gt 25
    echo "Can't bump above 25!"
    exit 1
end

echo "Bumping from $curr_num to $new_num!"

# aoc2020/src/main.rs
sed -i "s/\(NB_DAYS: usize = \)[0-9]\+/\1$new_num/" aoc2020/src/main.rs
sed -i "s|// \(&day$new_num::day,\)|\1|" aoc2020/src/main.rs

# aoc2020/Cargo.toml
sed -i "s/$curr_num.0.0/$new_num.0.0/" aoc2020/Cargo.toml
echo "day$new_num = {path = \"../day$new_num\"}" >>aoc2020/Cargo.toml

# .gitlab-ci.yml
sed -i "12s/]/, \"build-day-$new_num\"]/" .gitlab-ci.yml
echo "
# DAY $new_num
build-day-$new_num:
    stage: build
    needs: [\"build-utils\"]
    script:
        - cargo build -p day$new_num

test-day-$new_num:
    stage: test
    needs: [\"build-day-$new_num\"]
    script:
        - cargo test --lib -p day$new_num" >>.gitlab-ci.yml

# Cargo.toml
sed -i "s/^]/    \"day$new_num\",\n]/" Cargo.toml
