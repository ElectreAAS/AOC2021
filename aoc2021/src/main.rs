use std::env;
use std::time::{Duration, Instant};
use utils::types::*;

const NB_DAYS: usize = 25;

const FUNCTIONS: [&(dyn Fn(String) -> Return); NB_DAYS] = [
    &day1::day,
    &day2::day,
    &day3::day,
    &day4::day,
    &day5::day,
    &day6::day,
    &day7::day,
    &day8::day,
    &day9::day,
    &day10::day,
    &day11::day,
    &day12::day,
    &day13::day,
    &day14::day,
    &day15::day,
    &day16::day,
    &day17::day,
    &day18::day,
    &day19::day,
    &day20::day,
    &day21::day,
    &day22::day,
    &day23::day,
    &day24::day,
    &day25::day,
];

fn run(n: usize) -> Result<Duration> {
    let contents = utils::get_input(n)?;
    let before = Instant::now();
    let result = FUNCTIONS[n - 1](contents)?;
    let elapsed = before.elapsed();
    println!(
        "Day {:2} done in {:3}ms: {}",
        n,
        elapsed.as_millis(),
        result
    );
    Ok(elapsed)
}

pub fn dispatch(n: usize) -> Result<()> {
    match n {
        0 => {
            let mut time_all = Duration::ZERO;
            for n in 1..=NB_DAYS {
                time_all += run(n)?;
            }
            println!("Total: {}ms", time_all.as_millis());
        }
        1..=NB_DAYS => {
            run(n)?;
        }
        _ => {
            return Err(Error::Input(format!(
                "Please enter a number between 1 and {}",
                NB_DAYS,
            )))
        }
    };
    Ok(())
}

fn main() -> Result<()> {
    let err_msg = String::from("Please enter a number.");
    let args: Vec<_> = env::args().collect();
    let num = args.get(1).ok_or(Error::Input(err_msg))?;
    let n = num.parse()?;
    dispatch(n)
}
