pub mod types;
use std::fmt::Display;
use std::fs::File;
use std::io::Read;
use types::*;

pub fn min_max(lhs: usize, rhs: usize) -> (usize, usize) {
    if lhs > rhs {
        (rhs, lhs)
    } else {
        (lhs, rhs)
    }
}

pub fn abs_diff(lhs: usize, rhs: usize) -> usize {
    if lhs > rhs {
        lhs - rhs
    } else {
        rhs - lhs
    }
}

/// Gives you a list of the neighbours around spot (x, y).
///
/// This function assumes the search space is a square.
/// Parameter `max` is exclusive.
///
/// # Examples:
/// Without diagonals:
/// ```
/// let (x, y) = (1, 2);
/// let neighbours = utils::around(x, y, 10, false);
/// let should_be = vec![(1, 1), (0, 2), (2, 2), (1, 3)];
/// assert_eq!(neighbours, should_be);
/// ```
/// With diagonals and close to the edge:
/// ```
/// let (x, y) = (5, 10);
/// let neighbours = utils::around(x, y, 10, true);
/// let should_be = vec![(5, 9), (4, 9), (6, 9), (4, 10), (6, 10)];
/// assert_eq!(neighbours, should_be);
/// ```
pub fn around(x: usize, y: usize, max: usize, diagonals: bool) -> Vec<(usize, usize)> {
    let mut v = vec![];
    // Top
    if y > 0 {
        // Top center
        v.push((x, y - 1));
        if diagonals {
            // Top left
            if x > 0 {
                v.push((x - 1, y - 1));
            }
            // Top right
            if x < max - 1 {
                v.push((x + 1, y - 1));
            }
        }
    }
    // Left
    if x > 0 {
        v.push((x - 1, y));
    }
    // Right
    if x < max - 1 {
        v.push((x + 1, y));
    }
    // Bot
    if y < max - 1 {
        // Bot center
        v.push((x, y + 1));
        if diagonals {
            // Bot left
            if x > 0 {
                v.push((x - 1, y + 1));
            }
            // Bot right
            if x < max - 1 {
                v.push((x + 1, y + 1));
            }
        }
    }
    v
}

pub fn box_format(grid: &[Vec<impl Display>]) -> String {
    let width = grid.len();
    let elem_size = format!("{}", grid[0][0]).chars().count();
    let mut s = String::from("┌");
    for _ in 0..width {
        s.push('─');
        if elem_size > 1 {
            s.push_str(&"─".repeat(elem_size));
        }
    }
    if elem_size > 1 {
        s.push('─');
    }
    s.push_str("┐\n");
    for y in 0..grid[0].len() {
        s.push('│');
        for col in grid {
            let elem = &col[y];
            if elem_size > 1 {
                s.push_str(&format!(" {}", elem));
            } else {
                s.push_str(&format!("{}", elem));
            }
        }
        if elem_size > 1 {
            s.push(' ');
        }
        s.push_str("│\n");
    }
    s.push('└');
    for _ in 0..width {
        s.push('─');
        if elem_size > 1 {
            s.push_str(&"─".repeat(elem_size));
        }
    }
    if elem_size > 1 {
        s.push('─');
    }
    s.push('┘');
    s
}

fn slurp(file_name: &str) -> Result<String> {
    let mut f = File::open(file_name)?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

pub fn get_input(day: usize) -> Result<String> {
    let file_name = format!("day{}/input.txt", day);
    slurp(&file_name)
}

pub fn get_test() -> Result<String> {
    slurp("test.txt")
}

pub fn get_test_n(num: usize) -> Result<String> {
    let file_name = format!("test{}.txt", num);
    slurp(&file_name)
}
