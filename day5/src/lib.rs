use std::fmt;
use std::ops::{Deref, DerefMut};
use std::str::FromStr;
use utils::types::*;

enum Direction {
    Horizontal,
    Vertical,
    Diagonal,
}

struct Line {
    x1: usize,
    y1: usize,
    x2: usize,
    y2: usize,
    direction: Direction,
}

impl FromStr for Line {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let (from, to) = s
            .split_once(" -> ")
            .ok_or_else(|| Error::Parsing(s.to_string()))?;
        let (x1, y1) = from
            .split_once(',')
            .ok_or_else(|| Error::Parsing(from.to_string()))?;
        let (x2, y2) = to
            .split_once(',')
            .ok_or_else(|| Error::Parsing(to.to_string()))?;
        let x1 = x1.parse()?;
        let y1 = y1.parse()?;
        let x2 = x2.parse()?;
        let y2 = y2.parse()?;

        let direction = if y1 == y2 {
            Direction::Horizontal
        } else if x1 == x2 {
            Direction::Vertical
        } else {
            Direction::Diagonal
        };
        Ok(Line {
            x1,
            y1,
            x2,
            y2,
            direction,
        })
    }
}

impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "({:03}, {:03}) -> ({:03}, {:03})",
            self.x1, self.y1, self.x2, self.y2
        )
    }
}

impl Line {
    fn max(&self) -> usize {
        let mx = self.x1.max(self.x2);
        let my = self.y1.max(self.y2);
        mx.max(my)
    }

    fn path(&self) -> Vec<(usize, usize)> {
        let mut v = Vec::new();
        match self.direction {
            Direction::Horizontal => {
                let (min, max) = utils::min_max(self.x1, self.x2);
                for x in min..=max {
                    v.push((x, self.y1));
                }
            }
            Direction::Vertical => {
                let (min, max) = utils::min_max(self.y1, self.y2);
                for y in min..=max {
                    v.push((self.x1, y));
                }
            }
            Direction::Diagonal => {
                let x_values: Vec<usize> = if self.x1 < self.x2 {
                    (self.x1..=self.x2).collect()
                } else {
                    (self.x2..=self.x1).rev().collect()
                };
                let y_values: Vec<usize> = if self.y1 < self.y2 {
                    (self.y1..=self.y2).collect()
                } else {
                    (self.y2..=self.y1).rev().collect()
                };
                for (&x, y) in x_values.iter().zip(y_values) {
                    v.push((x, y));
                }
            }
        }
        v
    }
}

struct Grid(Vec<Vec<usize>>);

impl Deref for Grid {
    type Target = Vec<Vec<usize>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Grid {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let v: Vec<Vec<_>> = self
            .iter()
            .map(|line| {
                line.iter()
                    .map(|c| match c {
                        0 => ' ',
                        1 => '·',
                        _ => '*',
                    })
                    .collect()
            })
            .collect();
        utils::box_format(&v).fmt(f)
    }
}

impl Grid {
    fn default(size: usize) -> Grid {
        Grid(vec![vec![0; size]; size])
    }

    fn add_line(&mut self, line: Line) {
        for (x, y) in line.path() {
            self[x][y] += 1
        }
    }

    fn nb_danger(&self) -> usize {
        self.iter()
            .flatten()
            .fold(0, |sum, &case| if case > 1 { sum + 1 } else { sum })
    }
}

pub fn day(contents: String) -> Return {
    let mut size = 0;
    let lines = contents
        .lines()
        .map(|line| {
            let l: Line = line.parse()?;
            size = size.max(l.max());
            Ok(l)
        })
        .collect::<Result<Vec<_>>>()?;
    let mut grid = Grid::default(size + 1);
    for line in lines {
        grid.add_line(line);
    }
    ret(grid.nb_danger())
}

#[cfg(test)]
mod day5 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 12);
        Ok(())
    }
}
