use std::convert::TryFrom;
use std::str::FromStr;
use utils::types::*;

enum Length {
    /// Total length of the subpackets, in practice u15.
    TotalLength(usize),
    /// Number of subpackets, in practice u11.
    SubPacketsNum(usize),
}

impl FromStr for Length {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let length_id = u8::from_str_radix(&s[..1], 2)?;
        Ok(if length_id == 0 {
            Length::TotalLength(usize::from_str_radix(&s[1..16], 2)?)
        } else {
            Length::SubPacketsNum(usize::from_str_radix(&s[1..12], 2)?)
        })
    }
}

impl Length {
    fn size(&self) -> usize {
        1 + match self {
            Length::TotalLength(_) => 15,
            Length::SubPacketsNum(_) => 11,
        }
    }
}

enum Operator {
    /// 0
    Sum,
    /// 1
    Product,
    /// 2
    Minimum,
    /// 3
    Maximum,
    /// 5
    Greater,
    /// 6
    Lesser,
    /// 7
    Equal,
}

impl TryFrom<u8> for Operator {
    type Error = Error;

    fn try_from(num: u8) -> Result<Self> {
        match num {
            0 => Ok(Operator::Sum),
            1 => Ok(Operator::Product),
            2 => Ok(Operator::Minimum),
            3 => Ok(Operator::Maximum),
            5 => Ok(Operator::Greater),
            6 => Ok(Operator::Lesser),
            7 => Ok(Operator::Equal),
            n => Err(Error::Parsing(format!("Unknown type ID: {}", n))),
        }
    }
}

enum Contents {
    /// Literal value, type ID 4.
    /// Contains data, in practice Vec<u4>.
    Literal(Vec<u8>),
    /// Every type ID except 4.
    Command(Length, Operator, Vec<Packet>),
}

impl FromStr for Contents {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let type_id = u8::from_str_radix(&s[..3], 2)?;
        if type_id == 4 {
            // Literal value
            let mut v = vec![];
            let chars: Vec<char> = s[3..].chars().collect();
            let mut i = 0;
            loop {
                let s = format!(
                    "{}{}{}{}",
                    chars[i + 1],
                    chars[i + 2],
                    chars[i + 3],
                    chars[i + 4]
                );
                let n = u8::from_str_radix(&s, 2)?;
                v.push(n);
                if chars[i] == '0' {
                    break;
                }
                i += 5;
            }
            Ok(Contents::Literal(v))
        } else {
            // Operator
            let op = Operator::try_from(type_id)?;
            let len: Length = s[3..].parse()?;
            let mut from = 3 + len.size();
            let mut v = Vec::new();
            match len {
                Length::TotalLength(l) => {
                    let to = from + l;
                    while from < to {
                        let to_parse = &s[from..to];
                        let p: Packet = to_parse.parse()?;
                        from += p.size();
                        v.push(p);
                    }
                }
                Length::SubPacketsNum(n) => {
                    while v.len() != n {
                        let to_parse = &s[from..];
                        let p: Packet = to_parse.parse()?;
                        from += p.size();
                        v.push(p);
                    }
                }
            };
            Ok(Contents::Command(len, op, v))
        }
    }
}

impl Contents {
    fn size(&self) -> usize {
        3 + match self {
            Contents::Literal(v) => v.len() * 5,
            Contents::Command(l, _, v) => l.size() + v.iter().map(Packet::size).sum::<usize>(),
        }
    }

    fn value(&self) -> Result<usize> {
        match self {
            Contents::Literal(v) => {
                let binary: String = v.iter().map(|n| format!("{:04b}", n)).collect();
                usize::from_str_radix(&binary, 2)
                    .map_err(|_| unreachable("Literal contents should be a parsable binary number"))
            }
            Contents::Command(_, Operator::Sum, v) => Ok(v
                .iter()
                .map(Packet::value)
                .collect::<Result<Vec<_>>>()?
                .iter()
                .sum()),
            Contents::Command(_, Operator::Product, v) => Ok(v
                .iter()
                .map(Packet::value)
                .collect::<Result<Vec<_>>>()?
                .iter()
                .product()),
            Contents::Command(_, Operator::Minimum, v) => Ok(*v
                .iter()
                .map(Packet::value)
                .collect::<Result<Vec<_>>>()?
                .iter()
                .min()
                .ok_or_else(|| {
                    unreachable("Minimum operator should have positive number of arguments")
                })?),
            Contents::Command(_, Operator::Maximum, v) => Ok(*v
                .iter()
                .map(Packet::value)
                .collect::<Result<Vec<_>>>()?
                .iter()
                .max()
                .ok_or_else(|| {
                    unreachable("Maximum operator should have positive number of arguments")
                })?),
            Contents::Command(_, Operator::Greater, v) => {
                let left = v[0].value()?;
                let right = v[1].value()?;
                Ok((left > right).into())
            }
            Contents::Command(_, Operator::Lesser, v) => {
                let left = v[0].value()?;
                let right = v[1].value()?;
                Ok((left < right).into())
            }
            Contents::Command(_, Operator::Equal, v) => {
                let left = v[0].value()?;
                let right = v[1].value()?;
                Ok((left == right).into())
            }
        }
    }
}

struct Packet {
    contents: Contents,
}

impl FromStr for Packet {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        // The unused version number occupies the first 3 bits
        let contents = s[3..].parse()?;
        Ok(Packet { contents })
    }
}

impl Packet {
    fn size(&self) -> usize {
        3 + self.contents.size()
    }

    fn value(&self) -> Result<usize> {
        self.contents.value()
    }
}

pub fn day(contents: String) -> Return {
    let binary = contents
        .trim()
        .chars()
        .map(|ch| {
            let n = u16::from_str_radix(&ch.to_string(), 16)?;
            Ok(format!("{:04b}", n))
        })
        .collect::<Result<String>>()?;
    let p: Packet = binary.parse()?;
    ret(p.value()?)
}

#[cfg(test)]
mod day16 {
    use super::*;

    #[test]
    fn test_input_0() -> Result<()> {
        let contents = utils::get_test_n(0)?;
        assert_eq!(day(contents)?, 3);
        Ok(())
    }

    #[test]
    fn test_input_1() -> Result<()> {
        let contents = utils::get_test_n(1)?;
        assert_eq!(day(contents)?, 54);
        Ok(())
    }

    #[test]
    fn test_input_2() -> Result<()> {
        let contents = utils::get_test_n(2)?;
        assert_eq!(day(contents)?, 7);
        Ok(())
    }

    #[test]
    fn test_input_3() -> Result<()> {
        let contents = utils::get_test_n(3)?;
        assert_eq!(day(contents)?, 9);
        Ok(())
    }

    #[test]
    fn test_input_4() -> Result<()> {
        let contents = utils::get_test_n(4)?;
        assert_eq!(day(contents)?, 1);
        Ok(())
    }
}
