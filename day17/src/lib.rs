use utils::types::*;

/// x first, from -> to.
struct Area {
    from_x: isize,
    to_x: isize,
    from_y: isize,
    to_y: isize,
}

impl Area {
    /// Checks whether x is to the left of the area.
    fn is_left(&self, x: isize) -> bool {
        x < self.from_x
    }

    /// Checks whether a coord pair overshot the area.
    fn is_bot_right(&self, x: isize, y: isize) -> bool {
        y < self.from_y || x > self.to_x
    }

    /// Checks whether a coord pair is in the area.
    /// Only use after a check of is_bot_right.
    fn is_in(&self, x: isize, y: isize) -> bool {
        self.from_x <= x && y <= self.to_y
    }

    /// Checks whether the velocity pair will eventually reach the area.
    fn will_be_in(&self, mut vx: isize, mut vy: isize) -> bool {
        let mut x = 0;
        let mut y = 0;
        loop {
            if (vx <= 0 && self.is_left(x)) || self.is_bot_right(x, y) {
                return false;
            }
            if self.is_in(x, y) {
                return true;
            }
            x += vx;
            y += vy;
            match vx {
                n if n > 0 => vx -= 1,
                0 => (),
                _ => vx += 1,
            }
            vy -= 1;
        }
    }

    /// Brute-force the number of velocity pairs that eventually reach the target area.
    fn distinct_shots_in(&self) -> usize {
        let x_low = ((self.from_x * 2) as f64).sqrt() as isize;
        let x_high = self.to_x;
        let y_low = self.from_y;
        let y_high = -self.from_y - 1;

        let mut counter = 0;
        for x in x_low..=x_high {
            for y in y_low..=y_high {
                if self.will_be_in(x, y) {
                    counter += 1;
                }
            }
        }
        counter
    }
}

pub fn day(contents: String) -> Return {
    let nums: Vec<_> = contents
        .split(|c: char| c != '-' && !c.is_numeric())
        .filter_map(|s| s.parse::<isize>().ok())
        .collect();
    let area = Area {
        from_x: nums[0],
        to_x: nums[1],
        from_y: nums[2],
        to_y: nums[3],
    };
    ret(area.distinct_shots_in())
}

#[cfg(test)]
mod day17 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 112);
        Ok(())
    }
}
