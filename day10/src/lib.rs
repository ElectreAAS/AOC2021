use utils::types::*;

#[derive(PartialEq)]
enum DelimForm {
    /// ()
    Parenthesis,
    /// []
    SquareBrack,
    /// {}
    CurlyBrack,
    /// <>
    AngleBrack,
}

struct Delimiter {
    form: DelimForm,
    is_left: bool,
}

impl TryFrom<char> for Delimiter {
    type Error = Error;

    fn try_from(ch: char) -> Result<Self> {
        match ch {
            '(' => Ok(Delimiter {
                is_left: true,
                form: DelimForm::Parenthesis,
            }),
            ')' => Ok(Delimiter {
                is_left: false,
                form: DelimForm::Parenthesis,
            }),
            '[' => Ok(Delimiter {
                is_left: true,
                form: DelimForm::SquareBrack,
            }),
            ']' => Ok(Delimiter {
                is_left: false,
                form: DelimForm::SquareBrack,
            }),
            '{' => Ok(Delimiter {
                is_left: true,
                form: DelimForm::CurlyBrack,
            }),
            '}' => Ok(Delimiter {
                is_left: false,
                form: DelimForm::CurlyBrack,
            }),
            '<' => Ok(Delimiter {
                is_left: true,
                form: DelimForm::AngleBrack,
            }),
            '>' => Ok(Delimiter {
                is_left: false,
                form: DelimForm::AngleBrack,
            }),
            _ => Err(Error::Parsing(ch.to_string())),
        }
    }
}

impl From<Delimiter> for char {
    fn from(d: Delimiter) -> Self {
        match d.form {
            DelimForm::Parenthesis => {
                if d.is_left {
                    '('
                } else {
                    ')'
                }
            }
            DelimForm::SquareBrack => {
                if d.is_left {
                    '['
                } else {
                    ']'
                }
            }
            DelimForm::CurlyBrack => {
                if d.is_left {
                    '{'
                } else {
                    '}'
                }
            }
            DelimForm::AngleBrack => {
                if d.is_left {
                    '<'
                } else {
                    '>'
                }
            }
        }
    }
}

impl Delimiter {
    fn score(&self) -> usize {
        if self.is_left {
            match self.form {
                DelimForm::Parenthesis => 1,
                DelimForm::SquareBrack => 2,
                DelimForm::CurlyBrack => 3,
                DelimForm::AngleBrack => 4,
            }
        } else {
            match self.form {
                DelimForm::Parenthesis => 3,
                DelimForm::SquareBrack => 57,
                DelimForm::CurlyBrack => 1197,
                DelimForm::AngleBrack => 25137,
            }
        }
    }
}

pub fn day(contents: String) -> Return {
    let mut scores = vec![];
    for line in contents.lines() {
        let mut corrupted = false;
        let mut stack = vec![];
        for ch in line.chars() {
            let d: Delimiter = ch.try_into()?;
            if d.is_left {
                stack.push(d);
            } else if !matches!(stack.pop(), Some(popped) if popped.form == d.form) {
                corrupted = true;
                break;
            }
        }
        if !corrupted {
            let mut score = 0;
            while let Some(popped) = stack.pop() {
                score *= 5;
                score += popped.score();
            }
            scores.push(score);
        }
    }
    scores.sort_unstable();
    ret(scores[scores.len() / 2])
}

#[cfg(test)]
mod day10 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 288957);
        Ok(())
    }
}
