use std::collections::HashMap;
use std::str::FromStr;
use utils::types::*;

struct Network {
    small_caves: Vec<bool>,
    adj_list: Vec<Vec<usize>>,
    end_index: usize,
}

impl FromStr for Network {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut small_caves = vec![true];
        let mut adj_list = vec![vec![]];
        let mut name_to_num = HashMap::from([("start", 0)]);
        let mut next = 0;
        for line in s.lines() {
            let (left, right) = line
                .split_once('-')
                .ok_or_else(|| Error::Parsing(line.to_string()))?;
            let src = if let Some(n) = name_to_num.get(left) {
                *n
            } else {
                next += 1;
                name_to_num.insert(left, next);
                small_caves.push(is_small_cave(left));
                adj_list.push(Vec::new());
                next
            };
            let dst = if let Some(n) = name_to_num.get(right) {
                *n
            } else {
                next += 1;
                name_to_num.insert(right, next);
                small_caves.push(is_small_cave(right));
                adj_list.push(Vec::new());
                next
            };
            if left != "end" && right != "start" {
                adj_list[src].push(dst);
            }
            if right != "end" && left != "start" {
                adj_list[dst].push(src);
            }
        }
        let end_index = *name_to_num
            .get("end")
            .ok_or_else(|| Error::Parsing(s.to_string()))?;
        Ok(Network {
            small_caves,
            adj_list,
            end_index,
        })
    }
}

fn is_small_cave(name: &str) -> bool {
    name.find(char::is_lowercase) == Some(0)
}

fn dfs(start_node: usize, joker_used: bool, visited: &[bool], network: &Network) -> usize {
    if start_node == network.end_index {
        return 1;
    }
    let neighbours = &network.adj_list[start_node];
    let mut new_v = visited.to_owned();
    if network.small_caves[start_node] {
        new_v[start_node] = true;
    }
    let mut count = 0;
    for neigh in neighbours {
        if !visited[*neigh] {
            count += dfs(*neigh, joker_used, &new_v, network);
        } else if !joker_used {
            count += dfs(*neigh, true, &new_v, network);
        }
    }
    count
}

pub fn day(contents: String) -> Return {
    let network = contents.parse::<Network>()?;
    ret(dfs(
        0,
        false,
        &vec![false; network.small_caves.len()],
        &network,
    ))
}

#[cfg(test)]
mod day12 {
    use super::*;

    #[test]
    fn test_input_0() -> Result<()> {
        let contents = utils::get_test_n(0)?;
        assert_eq!(day(contents)?, 36);
        Ok(())
    }

    #[test]
    fn test_input_1() -> Result<()> {
        let contents = utils::get_test_n(1)?;
        assert_eq!(day(contents)?, 103);
        Ok(())
    }

    #[test]
    fn test_input_2() -> Result<()> {
        let contents = utils::get_test_n(2)?;
        assert_eq!(day(contents)?, 3509);
        Ok(())
    }
}
