use utils::types::*;

fn generate(generations: &mut [usize; 9]) {
    let newborns = generations[0];
    for i in 0..8 {
        generations[i] = generations[i + 1];
    }
    generations[6] += newborns;
    generations[8] = newborns;
}

pub fn day(contents: String) -> Return {
    let fishes = contents
        .trim()
        .split(',')
        .map(|s| Ok(s.parse::<usize>()?))
        .collect::<Result<Vec<_>>>()?;
    let mut generations = [0; 9];
    for f in fishes {
        generations[f] += 1;
    }
    for _ in 0..256 {
        generate(&mut generations);
    }
    ret::<usize>(generations.iter().sum())
}

#[cfg(test)]
mod day6 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 26984457539);
        Ok(())
    }
}
