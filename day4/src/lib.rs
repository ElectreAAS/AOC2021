use std::fmt;
use std::ops::{Deref, DerefMut};
use std::str::FromStr;
use utils::types::*;

#[derive(Clone, Copy, Default)]
struct Case {
    number: usize,
    marked: bool,
}

impl FromStr for Case {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let n = s.parse()?;
        Ok(Case {
            number: n,
            marked: false,
        })
    }
}

impl fmt::Display for Case {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c = if self.marked { 't' } else { 'f' };
        write!(f, "{:2}{}", self.number, c)
    }
}

struct Board([Case; 25]);

impl Deref for Board {
    type Target = [Case; 25];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Board {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl FromStr for Board {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        if s.split_whitespace().count() != 25 {
            return Err(Error::Parsing(s.to_string()));
        }
        let mut cases = [Case::default(); 25];
        for (i, n) in s.split_ascii_whitespace().enumerate() {
            cases[i] = n.parse()?;
        }
        Ok(Board(cases))
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut v = Vec::with_capacity(5);
        for x in 0..5 {
            v.push(Vec::with_capacity(5));
            for y in 0..5 {
                v[x].push(self[y * 5 + x]);
            }
        }
        utils::box_format(&v).fmt(f)
    }
}

impl Board {
    fn is_winning_line(&self, i: usize) -> bool {
        self[i * 5..i * 5 + 5].iter().all(|case| case.marked)
    }

    fn is_winning_col(&self, i: usize) -> bool {
        self.iter().skip(i).step_by(5).all(|case| case.marked)
    }

    fn is_winning(&self) -> bool {
        (0..5).any(|i| self.is_winning_line(i) || self.is_winning_col(i))
    }

    fn mark(&mut self, n: usize) -> bool {
        let mut changed = None;
        for i in 0..25 {
            if !self[i].marked && self[i].number == n {
                self[i].marked = true;
                changed = Some(i);
                break;
            }
        }
        match changed {
            None => false,
            Some(i) => {
                // If this is a win-inducing mark,
                if !(self.is_winning_line(i / 5) || self.is_winning_col(i % 5)) {
                    return false;
                }
                // and it wasn't winning already,
                self[i].marked = false;
                let won_before = self.is_winning();
                self[i].marked = true;
                // then it is winning for the first time.
                !won_before
            }
        }
    }

    fn sum_unmarked(&self) -> usize {
        self.iter()
            .fold(0, |sum, c| if !c.marked { sum + c.number } else { sum })
    }
}

pub fn day(contents: String) -> Return {
    let (nbs, rest) = contents
        .split_once('\n')
        .ok_or_else(|| Error::Parsing(contents.to_string()))?;
    let drawn_nbs = nbs
        .split(',')
        .map(|s| Ok(s.parse::<usize>()?))
        .collect::<Result<Vec<_>>>()?;
    let mut boards = rest
        .split("\n\n")
        .map(|board_str| board_str.parse::<Board>())
        .collect::<Result<Vec<_>>>()?;

    let nb_boards = boards.len();
    let mut winning_boards = 0;
    for n in drawn_nbs {
        for b in &mut boards {
            let is_winning = b.mark(n);
            if is_winning {
                winning_boards += 1;
                if winning_boards == nb_boards {
                    return ret(n * b.sum_unmarked());
                }
            }
        }
    }
    Err(unreachable("No winner at bingo?"))
}

#[cfg(test)]
mod day4 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 1924);
        Ok(())
    }
}
