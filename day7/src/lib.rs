use std::collections::HashMap;
use utils::types::*;

fn distance(n: usize, crabs: &[usize]) -> usize {
    crabs.iter().fold(0, |sum, &c| {
        let abs_dist = utils::abs_diff(n, c);
        sum + abs_dist * (abs_dist + 1) / 2
    })
}

/// This works under the (big but correct in experiments) assumption that there is only one local minimum.
fn find_min(max: usize, crabs: &[usize]) -> usize {
    let mut cursor = max / 2;
    let mut distances: HashMap<usize, usize> = HashMap::new();
    loop {
        let dist_prev = if cursor == 0 {
            usize::MAX
        } else {
            *distances
                .entry(cursor - 1)
                .or_insert_with(|| distance(cursor - 1, crabs))
        };
        let dist_self = *distances
            .entry(cursor)
            .or_insert_with(|| distance(cursor, crabs));
        let dist_next = if cursor == max - 1 {
            usize::MAX
        } else {
            *distances
                .entry(cursor + 1)
                .or_insert_with(|| distance(cursor + 1, crabs))
        };
        if dist_self > dist_prev {
            cursor -= 1;
        } else if dist_self > dist_next {
            cursor += 1;
        } else {
            return dist_self;
        }
    }
}

pub fn day(contents: String) -> Return {
    let mut max = usize::MIN;
    let crabs = contents
        .trim()
        .split(',')
        .map(|s| {
            let crab: usize = s.parse()?;
            max = max.max(crab);
            Ok(crab)
        })
        .collect::<Result<Vec<_>>>()?;
    ret(find_min(max, &crabs))
}

#[cfg(test)]
mod day7 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 168);
        Ok(())
    }
}
