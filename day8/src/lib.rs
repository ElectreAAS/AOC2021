use std::str::FromStr;
use utils::types::*;

struct Line {
    input: Vec<String>,
    output: Vec<String>,
}

impl FromStr for Line {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let (before, after) = s
            .split_once('|')
            .ok_or_else(|| Error::Parsing(s.to_string()))?;
        let input = before.split_whitespace().map(String::from).collect();
        let output = after.split_whitespace().map(String::from).collect();
        Ok(Line { input, output })
    }
}

impl Line {
    fn solve(&self) -> Result<usize> {
        // Setup: possibilities
        let mut a = String::from("abcdefg");
        let mut b;
        let mut c = a.clone();
        let mut d;
        let mut e;
        let mut f;
        let mut g;

        // First step: find the code for 1
        let one = self
            .input
            .iter()
            .find(|s| s.len() == 2)
            .ok_or_else(|| unreachable("No input has len 2"))?;
        // This reduces possibilites for c and f to the chars in one
        c.retain(|ch| one.contains(ch));
        f = c.clone();
        // and for everyone else to those not in one
        a.retain(|ch| !one.contains(ch));
        b = a.clone();

        // Second step: find the code for 7
        let seven = self
            .input
            .iter()
            .find(|s| s.len() == 3)
            .ok_or_else(|| unreachable("No input has len 3"))?;
        // This solves a, as it is the only character remaining
        a.retain(|ch| seven.contains(ch));
        // so we remove that possiblity for everyone else
        b.retain(|ch| !a.contains(ch));
        e = b.clone();

        // Third step: find the code for 4
        let four = self
            .input
            .iter()
            .find(|s| s.len() == 4)
            .ok_or_else(|| unreachable("No input has len 4"))?;
        // This reduces possibilites for b and d to the chars in 4
        b.retain(|ch| four.contains(ch));
        d = b.clone();
        // so we remove that possibility for everyone else
        e.retain(|ch| !b.contains(ch));
        g = e.clone();

        // Fourth step: find the code for 0
        let bd: Vec<_> = b.chars().collect();
        let zero = self
            .input
            .iter()
            .find(|s| {
                // 0 contains b but not d
                s.len() == 6 && (s.contains(bd[0]) != s.contains(bd[1]))
            })
            .ok_or_else(|| unreachable("No input fits the bill for zero"))?;
        // This solves b
        b.retain(|ch| zero.contains(ch));
        // and d
        d.retain(|ch| !b.contains(ch));

        // Fifth step: find the code for 6
        let cf: Vec<_> = c.chars().collect();
        let six = self
            .input
            .iter()
            .find(|s| {
                // 6 contains f but not c
                s.len() == 6 && (s.contains(cf[0]) != s.contains(cf[1]))
            })
            .ok_or_else(|| unreachable("No input fits the bill for six"))?;
        // This solves f
        f.retain(|ch| six.contains(ch));
        // and c
        c.retain(|ch| !f.contains(ch));

        // Sixth and final step of this process: find the code for 9
        let eg: Vec<_> = e.chars().collect();
        let nine = self
            .input
            .iter()
            .find(|s| {
                // 9 contains g but not e
                s.len() == 6 && (s.contains(eg[0]) != s.contains(eg[1]))
            })
            .ok_or_else(|| unreachable("No input fits the bill for nine"))?;
        // This solves g
        g.retain(|ch| nine.contains(ch));
        // and e
        e.retain(|ch| !g.contains(ch));

        // Now that all signals are properly mapped, we can decode output
        let string_res: String = self
            .output
            .iter()
            .map(|out| match out.len() {
                2 => '1',
                3 => '7',
                4 => '4',
                5 => {
                    if out.contains(&e) {
                        return '2';
                    }
                    if out.contains(&c) {
                        return '3';
                    }
                    '5'
                }
                6 => {
                    if !out.contains(&d) {
                        return '0';
                    }
                    if out.contains(&e) {
                        return '6';
                    }
                    '9'
                }
                _ => '8',
            })
            .collect();
        string_res
            .parse::<usize>()
            .map_err(|_| unreachable("End result should be a parsable usize :("))
    }
}

pub fn day(contents: String) -> Return {
    ret(contents.lines().try_fold(0, |sum, line| {
        let l: Line = line.parse()?;
        Ok::<_, Error>(sum + l.solve()?)
    })?)
}

#[cfg(test)]
mod day8 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 61229);
        Ok(())
    }
}
