use std::cmp::Reverse;
use std::collections::BinaryHeap;
use std::fmt;
use std::iter::Iterator;
use std::ops::Add;
use std::ops::{Deref, DerefMut};
use std::str::FromStr;
use std::thread;
use utils::types::*;

const NUM_SIZE: usize = 64;

const MAG_MULTIPLIERS: [usize; NUM_SIZE / 2] = {
    let mut mag_multipliers = [0; NUM_SIZE / 2];
    let mut i = 2;
    while i < NUM_SIZE / 2 {
        let mut value = 1;
        let mut index = i;
        while index > 1 {
            value *= 3 - (index % 2);
            index /= 2;
        }
        mag_multipliers[i] = value;
        i += 1;
    }
    mag_multipliers
};

const OFFSETS: [(usize, usize); NUM_SIZE / 2] = {
    let mut offsets = [(0, 0); NUM_SIZE / 2];
    let mut depth = 1;
    while depth < 5 {
        let floor_leftmost = 1 << depth;
        let floor_right = floor_leftmost << 1;
        let mut index = floor_leftmost;
        while index < floor_right {
            let right = floor_right + index;
            let left = right - floor_leftmost;
            offsets[index] = (left, right);
            index += 1;
        }
        depth += 1;
    }
    offsets
};

#[derive(Clone, Copy, PartialEq)]
struct Number([Option<usize>; NUM_SIZE]);

struct NumberIter<'a> {
    array: &'a Number,
    index: usize,
}

impl<'a> Iterator for NumberIter<'a> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        if self.index <= 1 {
            return None;
        }
        self.index -= 1;
        match self.array[self.index + 1] {
            None => self.next(),
            Some(n) => Some((self.index + 1, n)),
        }
    }
}

impl Deref for Number {
    type Target = [Option<usize>; NUM_SIZE];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Number {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl FromStr for Number {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut num = Number([None; NUM_SIZE]);
        let mut index = 1;
        for c in s.chars() {
            match c {
                '[' => index <<= 1,

                ']' => index >>= 1,
                ',' => index += 1,
                _ => {
                    let n = c.to_string().parse()?;
                    num[index] = Some(n);
                }
            }
        }
        Ok(num)
    }
}

impl fmt::Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::from("[ ");
        for x in **self {
            match x {
                None => s.push_str("_ "),
                Some(n) => s.push_str(&format!("{} ", n)),
            }
        }
        s.push(']');
        s.fmt(f)
    }
}

impl Add for Number {
    type Output = Self;

    fn add(mut self, other: Self) -> Self {
        self.merge(other);
        self
    }
}

impl Number {
    fn iter(&self) -> NumberIter {
        NumberIter {
            array: self,
            index: NUM_SIZE - 1,
        }
    }

    fn magnitude(&mut self) -> Result<usize> {
        self.reduce()?;
        Ok(self.iter().map(|(i, n)| MAG_MULTIPLIERS[i] * n).sum())
    }

    /// Rotate a tree either to the left or to the right, making its root the child of the new root.
    fn rotate(&mut self, is_left: bool) {
        let it: Vec<_> = self.iter().collect();
        for (index, n) in it {
            let new_index = if is_left {
                OFFSETS[index].0
            } else {
                OFFSETS[index].1
            };
            self[new_index] = Some(n);
            self[index] = None;
        }
    }

    /// Merge two trees.
    fn merge(&mut self, mut other: Self) {
        self.rotate(true);
        other.rotate(false);
        for (index, n) in other.iter() {
            // assert!(self[index].is_none());
            self[index] = Some(n);
        }
    }

    fn find_sibling(&self, is_left: bool, mut index: usize) -> Option<usize> {
        while (index & 1 == 0) == is_left {
            index >>= 1;
        }
        if index <= 1 {
            return None;
        }
        if is_left {
            index -= 1;
        } else {
            index += 1;
        }
        while self[index].is_none() {
            if index >= NUM_SIZE / 2 {
                return None;
            }
            index <<= 1;
            if is_left {
                index += 1;
            }
        }
        Some(index)
    }

    fn explode(
        &mut self,
        parent_index: usize,
        to_split: &mut BinaryHeap<Reverse<usize>>,
    ) -> Result<()> {
        let index = parent_index * 2;
        let left =
            self[index].ok_or_else(|| unreachable("Explode target should have a left child"))?;
        let right = self[index + 1]
            .ok_or_else(|| unreachable("Explode target should have a right child"))?;
        if let Some(i) = self.find_sibling(true, parent_index) {
            let value = left
                + self[i]
                    .ok_or_else(|| unreachable("Find sibling should return a suitable result"))?;
            if value >= 10 && i < NUM_SIZE / 2 {
                to_split.push(Reverse(i));
            }
            self[i] = Some(value);
        }
        if let Some(j) = self.find_sibling(false, parent_index) {
            let value = right
                + self[j]
                    .ok_or_else(|| unreachable("Find sibling should return a suitable result"))?;
            if value >= 10 && j < NUM_SIZE / 2 {
                to_split.push(Reverse(j));
            }
            self[j] = Some(value);
        }
        self[index] = None;
        self[index + 1] = None;
        self[parent_index] = Some(0);
        Ok(())
    }

    /// Perform every explosion on the fifth floor in order.
    fn explode_fifth(&mut self, to_split: &mut BinaryHeap<Reverse<usize>>) -> Result<()> {
        for index in ((NUM_SIZE / 2)..NUM_SIZE).step_by(2) {
            if self[index].is_some() {
                self.explode(index >> 1, to_split)?;
            }
        }
        Ok(())
    }

    fn split(&mut self, mut to_split: BinaryHeap<Reverse<usize>>) -> Result<()> {
        while let Some(index) = to_split.pop() {
            let index = index.0;
            match self[index] {
                Some(curr) if curr >= 10 => {
                    let left = curr >> 1;
                    let right = left + (curr & 1);
                    self[index << 1] = Some(left);
                    self[(index << 1) + 1] = Some(right);
                    if index >= NUM_SIZE / 4 {
                        // This split would produce an explosion, we trigger it right away
                        self.explode(index, &mut to_split)?;
                    } else {
                        self[index] = None;
                    }
                }
                _ => (),
            }
        }
        Ok(())
    }

    fn reduce(&mut self) -> Result<()> {
        let mut to_split = BinaryHeap::new();
        self.explode_fifth(&mut to_split)?;
        self.split(to_split)
    }
}

pub fn day(contents: String) -> Return {
    let numbers = contents
        .lines()
        .map(Number::from_str)
        .collect::<Result<Vec<_>>>()?;

    let pairs_len = numbers.len() * (numbers.len() - 1);
    let mut pairs = Vec::with_capacity(pairs_len);
    for x in numbers.iter() {
        for y in numbers.iter() {
            if x == y {
                continue;
            }
            pairs.push((*x, *y));
        }
    }

    let nb_thread = 4;
    let mut handles = Vec::with_capacity(nb_thread);
    for i in 1..=nb_thread {
        let chunk = pairs.split_off((nb_thread - i) * pairs_len / nb_thread);
        let h = thread::spawn(move || {
            let mut local_count = 0;
            for (x, y) in chunk {
                local_count = local_count.max((x + y).magnitude()?);
                local_count = local_count.max((y + x).magnitude()?);
            }
            Ok::<_, Error>(local_count)
        });
        handles.push(h);
    }
    let mut max = usize::MIN;
    for h in handles {
        max = max.max(h.join()??);
    }
    ret(max)
}

#[cfg(test)]
mod day18 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 3993);
        Ok(())
    }
}
