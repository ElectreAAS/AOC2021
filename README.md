# Advent of Code 2021
[Advent of code](https://adventofcode.com/) is a recurring set of programming puzzles published every year at the start of december.

I heard about it for the first time a few days into december and decided to tackle everything in Rust. In this repository are my solutions to all 25 problems of this year. <br>
 Note that those problems (save one) all have two parts, the second being generally an expansion on the rules of the puzzle, making it more complex. You have to solve part 1 to see what the part 2 is about, making the "brute-force first then think second" approach very well-suited. <br>
 With that in mind, only my solutions for part 2 of each day are present, as the part 1 were generally just plain bad code (or no code at all in certain cases).

## Design choices
 - I wanted to do everything in Rust to get more practice on it, and more specifically only using the standard library, and no `unsafe` code.
 - Total runtime should be under 1 second. This is achieved as the total printed when running everything on my 6 year old laptop is around 940ms.
 - No `panic`, `unwrap`, or anything of the sort. I use Rust's version of the error monad: `Result<T, E>`, along with a generous usage of the magnificent `?` operator.

## Special day notes
 - Days 1 to 5 are the warmup ones, translating the rules is enough to get it all up and running.
 6. Day 6 is where you can't just do it mindlessly. The naive approach would allocate each and every lanternfish and iterate over them. As the iteration number grows a lot and the exponentiation kicks in, you'll have to realize that every fish of a given generation (the same number of days until they create a new fish) is identical to every other one. You can then model the number of fishes at each step instead of counting them all individually.

 7. This is the first of the many times we'll need to use the well-known formula: `1+2+3+...n = n(n+1)/2`. Keep that one fresh in mind.
 Along with the formula, my solution uses a hypothesis that isn't present in the puzzle text but that I figured out while testing: there are no local minima other than the total minimum. That is, if for a given n, dist(n-1) > dist(n) && dist(n) < dist(n+1), we have reached our solution.

 8. This one is very tedious but follows naturally from the puzzle text.

 9. My solution doesn't use anything fancy, I just register in each case which basin it is a part of, and in a separate vector I store the size of each basin. <br>
 Basins are explored thoroughly in one go: upon arrival on an unmarked case different from 9, we send the recursive `expand_basin` function to map it all. The final calculation is trivial with iterators.

 10. Today class, we're gonna learn about stacks. You can push and pop them. That's about it.

 11. A 2-dimensional array of octopuses flashing left and right. Apart from remembering who already flashed this turn, the naive solution works just well.

 12. This is where it gets interesting: graph algorithms! Slow start for this one, but still. I store the graph as an adjacency list after translating the cave names into integers for ease of use. Using a boolean vector for the 'already visited' set works perfectly. Other than that it is a classic DFS.

 13. The real algorithm isn't the most interesting piece here, it is the formatting! Using the ASCII block character (`█`) and spaces instead of ugly `#` and `.` will allow you to actually see what your result looks like. On top of that I added a very tedious character recognition map to have the answer in exactly the expected format.

 14. Having comprehensible type aliases sure helped here. Other than that a simple map does the job.

 15. Second installment of graph algorithms! This time it's the shortest path one. I chose A* as a starting point as we can use the Manhattan distance for an admissible heuristic, and modified it a bit since the maximum f_score difference between the current node and a node in the openset is strongly bounded by a small number (here 11). Using 11 buckets (or bins) and BFS inside of them, I get a very reasonable run time. The map expansion part is annoying but almost entirely pre-computable, hence the enormous arrays.

 16. This one was horrible to debug as the only thing you're manipulating are binary numbers. I made extensive use of enums and structs here, along with their FromStr traits, as in the end the real challenge was the parsing phase.

 17. Time to use that formula we kept around. The first thing to note here is that, if we fire upward with a velocity y, the peak will be of height `y(y+1)/2` and after that the y velocity will start to increase again in negative. At the time where the probe reaches the surface of the water, the velocity is of -y (since the upwards and downwards phase take exactly the same amount of time). So on the step right after that, the depth of the probe will be of y. With a similar (but even simpler) calculation on x, we have rough bounds on the search space for all possible good shots. <br>
 [Legend says](https://www.reddit.com/r/adventofcode/comments/rily4v/2021_day_17_part_2_never_brute_force_when_you_can/) you can restrict the search space down to absolutely no uncertainty, but the rough bounds + brute-force trying of all shots gives us a negligible runtime already.

 18. **Snailfish numbers are actually binary trees**. Understanding that helps a lot with the implementation. Rust isn't the cleanest with recursive (potentially infinite) data structures. So we use the strict bound on depth (it cannot be more than 5) to use an array of static size instead. Using array indexes instead of recursion leads to tedious index-bit-shifting operations, but incredible performance.

 19. This one is a nightmare to reason about. Trying to write a naive algorithm leads to 5 nested loops of non-negligible amplitude. At least we can use `break`s... <br>
 There are comments in the code but one thing to note is that, when trying to rotate+offset a scanner the correct way, we may not yet have enough information in our truth sources, so we need to add it to the back of the queue to be processed again.

 20. In this challenge the gap between the sample and the actual input is the most brutal. In the sample, the first character in the enhancement algorithm is a `.`, whereas in the real input it is a `#`. This tiny change means a lot: the image of infinite size isn't inert! It actually flashes up and down every step. This means we have to have careful checks around the bounds of our expanding image. Once again, printing `█` box characters makes it very pretty in the end.

 21. Caching is the name of the game here, as the number of parallel universes created is staggering. Weighing the dice results reduces the exponentiation from 27 to 7, but still.

 22. A bit of geometry never hurt anyone. The intersection of two cuboids on the same grid is also a cuboid, thankfully. Here I add either positive or negative cuboids to my list and compute the intersection between each of them and a new one to be added to the list. Most of the work was drawing pretty boxes on a paper and trying to think of all the edge cases.

 23. Contrary to what the presentation of this puzzle implies, this is also a graph algorithm. Mapping a state to a node and possible moves to neighbours, and we have another A* on our hands! The heuristic calculations are where the complexity lies. I went for a 'pretend everyone can just move through everyone else' best-case-scenario, and it works okay. Of course before that I did a little interactive solver for part 1 just in case part 2 completely changed the rules. Thankfully my first try at doing it by hand got me the correct result, I don't know where I'd be otherwise...

 24. Again this isn't what it may imply. Simulating the ALU and executing the instructions one by one seems like such a common problem that we may forget to actually look at the (ugly) opcodes we have. The input is a sequence of 18-length blocks that all kinda do the same thing: input a new value in w, do weird calculations with 26 everywhere and add a bit of stuff to z. <br>
 Realizing that we can run every block on their own instead of one after the other changes everything (and saves you CPU too!). Knowing that in the end we need a value of 0 in z, we can backtrack the different input possibilites and create easily solvable equations. In the end we don't need to multiply or divide anything by 26, just to realize that we'll need exactly one more division than multiplication to arrive at 0. Hence the mapping mult -> push, div -> pop. It's a stack!

 25. Christmas morning calls for a simple puzzle. This one only has one part, that can be brute-forced in a reasonable amount of time. Or it can be used to make [pretty visualisations](https://www.reddit.com/r/adventofcode/comments/ro3bc4/2021_day_25_moving_sea_cucumbers/) (not my creation), up to you.
