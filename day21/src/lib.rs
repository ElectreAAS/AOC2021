use std::collections::HashMap;
use utils::types::*;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct GameState {
    p1_pos: u8,
    p2_pos: u8,
    p1_score: usize,
    p2_score: usize,
    p1_turn: bool,
    roll: usize,
}

/// Rolling three independent 3-sided dice will give you results from 3 to 9 with these frequencies (out of the 27 possible orders).
const ROLL_PROBS: [(usize, usize); 7] = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)];

fn universe(state: GameState, cache: &mut HashMap<GameState, (usize, usize)>) -> (usize, usize) {
    if let Some(res) = cache.get(&state) {
        return *res;
    }
    let mut new_state = state.clone();
    let (pos, score) = if state.p1_turn {
        (&mut new_state.p1_pos, &mut new_state.p1_score)
    } else {
        (&mut new_state.p2_pos, &mut new_state.p2_score)
    };
    *pos += state.roll as u8;
    if *pos > 10 {
        *pos -= 10;
    }
    *score += *pos as usize;
    if *score >= 21 {
        let res = if state.p1_turn { (1, 0) } else { (0, 1) };
        cache.insert(state, res);
        return res;
    }
    new_state.p1_turn = !state.p1_turn;
    let mut p1_score = 0;
    let mut p2_score = 0;
    for (new_roll, weight) in ROLL_PROBS {
        let mut loop_state = new_state.clone();
        loop_state.roll = new_roll;
        let (p1, p2) = universe(loop_state, cache);
        p1_score += p1 * weight;
        p2_score += p2 * weight;
    }
    cache.insert(state, (p1_score, p2_score));
    (p1_score, p2_score)
}

fn parse_start(contents: String) -> Result<(u8, u8)> {
    let nums: Vec<_> = contents
        .split_whitespace()
        .filter_map(|s| s.parse::<u8>().ok())
        .collect();
    if nums.len() != 4 || nums[0] != 1 || nums[2] != 2 {
        return Err(Error::Parsing(contents));
    }
    Ok((nums[1], nums[3]))
}

pub fn day(contents: String) -> Return {
    let (p1_pos, p2_pos) = parse_start(contents)?;
    let mut cache = HashMap::new();
    let mut p1_score = 0;
    let mut p2_score = 0;
    for (roll, weight) in ROLL_PROBS {
        let state = GameState {
            p1_pos,
            p2_pos,
            p1_score: 0,
            p2_score: 0,
            p1_turn: true,
            roll,
        };
        let (p1, p2) = universe(state, &mut cache);
        p1_score += p1 * weight;
        p2_score += p2 * weight;
    }
    ret(p1_score.max(p2_score))
}

#[cfg(test)]
mod day21 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 444356092776315);
        Ok(())
    }
}
