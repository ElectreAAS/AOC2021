use std::collections::HashMap;
use utils::types::*;

type Pair = [char; 2];
type Template = HashMap<Pair, usize>;
type Rules = HashMap<Pair, (Pair, Pair)>;

fn parse_all(contents: String) -> (char, Template, Rules) {
    let lines: Vec<Vec<char>> = contents
        .lines()
        .map(|line| line.chars().collect())
        .collect();
    let mut template: Template = HashMap::new();
    for win in lines[0].windows(2) {
        *template.entry([win[0], win[1]]).or_insert(0) += 1;
    }
    let mut rules: Rules = HashMap::new();
    for chars in &lines[2..] {
        let from = [chars[0], chars[1]];
        let to = ([chars[0], chars[6]], [chars[6], chars[1]]);
        rules.insert(from, to);
    }
    (lines[0][0], template, rules)
}

fn step(template: &Template, rules: &Rules) -> Result<Template> {
    let mut m = HashMap::new();
    for (k, v) in template {
        let (l, r) = rules
            .get(k)
            .ok_or_else(|| unreachable("Template key should be in rule map"))?;
        *m.entry(*l).or_insert(0) += v;
        *m.entry(*r).or_insert(0) += v;
    }
    Ok(m)
}

pub fn day(contents: String) -> Return {
    let (first, mut template, rules) = parse_all(contents);
    for _ in 0..40 {
        template = step(&template, &rules)?;
    }
    let mut freqs = HashMap::new();
    // We count here the first character of the entire thing
    freqs.insert(first, 1);
    let mut min_k = usize::MAX;
    let mut max_k = usize::MIN;
    for (pair, n) in template {
        // We only count the second half of the pair
        *freqs.entry(pair[1]).or_insert(0) += n;
    }
    for (_, v) in freqs {
        min_k = min_k.min(v);
        max_k = max_k.max(v);
    }
    ret(max_k - min_k)
}

#[cfg(test)]
mod day14 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 2188189693529);
        Ok(())
    }
}
