use std::fmt;
use std::str::FromStr;
use utils::types::*;

struct Cuboid {
    on_off: bool,
    x: (isize, isize),
    y: (isize, isize),
    z: (isize, isize),
}

impl FromStr for Cuboid {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let (on_off_str, rest) = s
            .split_once(' ')
            .ok_or_else(|| Error::Parsing(s.to_string()))?;
        let on_off = match on_off_str {
            "on" => true,
            "off" => false,
            _ => {
                return Err(Error::Parsing(s.to_string()));
            }
        };
        let rest: Vec<_> = rest
            .split(|c: char| c != '-' && !c.is_numeric())
            .filter_map(|s| s.parse::<isize>().ok())
            .collect();
        if rest.len() != 6 {
            return Err(Error::Parsing(s.to_string()));
        }
        let x = (rest[0], rest[1]);
        let y = (rest[2], rest[3]);
        let z = (rest[4], rest[5]);

        Ok(Cuboid { on_off, x, y, z })
    }
}

impl fmt::Display for Cuboid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let on_off_str = if self.on_off { "on" } else { "off" };
        write!(
            f,
            "{} x={}..{},y={}..{},z={}..{}",
            on_off_str, self.x.0, self.x.1, self.y.0, self.y.1, self.z.0, self.z.1
        )
    }
}

impl Cuboid {
    fn volume(&self) -> isize {
        let v = (self.x.1 - self.x.0 + 1) * (self.y.1 - self.y.0 + 1) * (self.z.1 - self.z.0 + 1);
        if self.on_off {
            v
        } else {
            -v
        }
    }

    fn intersection(&self, other: &Self) -> Option<Self> {
        let x = (self.x.0.max(other.x.0), self.x.1.min(other.x.1));
        if x.0 > x.1 {
            return None;
        }
        let y = (self.y.0.max(other.y.0), self.y.1.min(other.y.1));
        if y.0 > y.1 {
            return None;
        }
        let z = (self.z.0.max(other.z.0), self.z.1.min(other.z.1));
        if z.0 > z.1 {
            return None;
        }
        Some(Cuboid {
            on_off: !self.on_off,
            x,
            y,
            z,
        })
    }
}

pub fn day(contents: String) -> Return {
    let cubes = contents
        .lines()
        .map(Cuboid::from_str)
        .collect::<Result<Vec<_>>>()?;
    let mut reactor: Vec<Cuboid> = Vec::new();
    let mut intersections = Vec::new();
    for cuboid in cubes {
        for core in reactor.iter() {
            if let Some(inter) = core.intersection(&cuboid) {
                intersections.push(inter);
            }
        }
        if cuboid.on_off {
            reactor.push(cuboid);
        }
        reactor.append(&mut intersections);
    }
    let mut count = 0;
    for cuboid in reactor {
        count += cuboid.volume();
    }
    ret(count as usize)
}

#[cfg(test)]
mod day22 {
    use super::*;

    #[test]
    fn test_input_0() -> Result<()> {
        let contents = utils::get_test_n(0)?;
        assert_eq!(day(contents)?, 2758514936282235);
        Ok(())
    }

    #[test]
    fn test_input_1() -> Result<()> {
        let contents = utils::get_test_n(1)?;
        assert_eq!(day(contents)?, 4600);
        Ok(())
    }
}
