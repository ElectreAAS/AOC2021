use std::fmt;
use std::ops::{Deref, DerefMut};
use std::str::FromStr;
use utils::types::*;

#[derive(Clone, Copy, PartialEq)]
enum Spot {
    Right,
    Down,
    Empty,
}

impl TryFrom<char> for Spot {
    type Error = Error;

    fn try_from(ch: char) -> Result<Self> {
        match ch {
            '>' => Ok(Self::Right),
            'v' => Ok(Self::Down),
            '.' => Ok(Self::Empty),
            _ => Err(Error::Parsing(ch.to_string())),
        }
    }
}

impl From<Spot> for char {
    fn from(s: Spot) -> Self {
        match s {
            Spot::Right => '>',
            Spot::Down => 'v',
            Spot::Empty => '⋅',
        }
    }
}

struct Grid(Vec<Vec<Spot>>);

impl FromStr for Grid {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let len = s.find('\n').ok_or_else(|| Error::Parsing(s.to_string()))?;
        let mut grid = vec![Vec::with_capacity(len); len];
        for line in s.lines() {
            for (x, ch) in line.chars().enumerate() {
                grid[x].push(Spot::try_from(ch)?);
            }
        }
        for column in grid.iter_mut() {
            column.shrink_to_fit();
        }
        Ok(Grid(grid))
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = String::new();
        for y in 0..self[0].len() {
            for x in 0..self.len() {
                s.push(self[x][y].into());
            }
            s.push('\n');
        }
        s.fmt(f)
    }
}

impl Deref for Grid {
    type Target = Vec<Vec<Spot>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Grid {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

fn first_null_step(mut grid: Grid) -> usize {
    let width = grid.len();
    let height = grid[0].len();
    let mut step = 1;
    loop {
        let mut moved = false;
        {
            // Right
            for y in 0..height {
                let wrapping_right = grid[width - 1][y] == Spot::Right && grid[0][y] == Spot::Empty;
                let mut x = 0;
                while x < width - 1 {
                    if grid[x][y] == Spot::Right && grid[x + 1][y] == Spot::Empty {
                        grid[x][y] = Spot::Empty;
                        grid[x + 1][y] = Spot::Right;
                        x += 1;
                        moved = true;
                    }
                    x += 1;
                }
                if wrapping_right {
                    grid[width - 1][y] = Spot::Empty;
                    grid[0][y] = Spot::Right;
                    moved = true;
                }
            }
        }
        {
            // Down
            for x in 0..width {
                let wrapping_down = grid[x][height - 1] == Spot::Down && grid[x][0] == Spot::Empty;
                let mut y = 0;
                while y < height - 1 {
                    if grid[x][y] == Spot::Down && grid[x][y + 1] == Spot::Empty {
                        grid[x][y] = Spot::Empty;
                        grid[x][y + 1] = Spot::Down;
                        y += 1;
                        moved = true;
                    }
                    y += 1;
                }
                if wrapping_down {
                    grid[x][height - 1] = Spot::Empty;
                    grid[x][0] = Spot::Down;
                    moved = true;
                }
            }
        }

        if !moved {
            return step;
        }
        step += 1;
    }
}

pub fn day(contents: String) -> Return {
    let grid = contents.parse()?;
    ret(first_null_step(grid))
}

#[cfg(test)]
mod day25 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 58);
        Ok(())
    }
}
