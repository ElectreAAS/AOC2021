use std::convert::From;
use std::fmt;

#[derive(PartialEq)]
pub enum Error {
    /// The user produced invalid input.
    Input(String),
    /// The filesystem isn't being cooperative.
    FileSystem(String),
    Parsing(String),
    /// I made a mistake somewhere.
    Logic(String),
    /// A thread failed on its own.
    Thread,
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Input(s) => write!(f, "Input error {{{}}}", s),
            Self::FileSystem(s) => write!(f, "File system error {{{}}}", s),
            Self::Parsing(s) => write!(f, "Error while parsing: {{{}}}", s),
            Self::Logic(s) => write!(f, "Not as unreachable as you might think: {}", s),
            Self::Thread => write!(f, "Joined thread crashed in an unspecified way."),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::FileSystem(e.to_string())
    }
}

impl From<std::num::ParseIntError> for Error {
    fn from(e: std::num::ParseIntError) -> Self {
        Self::Parsing(e.to_string())
    }
}

impl From<Box<dyn std::any::Any + Send>> for Error {
    fn from(_: Box<dyn std::any::Any + Send>) -> Self {
        Self::Thread
    }
}

pub fn unreachable(s: &str) -> Error {
    Error::Logic(String::from(s))
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum RetVal {
    IntRet(usize),
    StrRet(String),
}

impl From<usize> for RetVal {
    fn from(n: usize) -> Self {
        Self::IntRet(n)
    }
}

impl From<String> for RetVal {
    fn from(s: String) -> Self {
        Self::StrRet(s)
    }
}

impl fmt::Display for RetVal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::IntRet(n) => n.fmt(f),
            Self::StrRet(s) => s.fmt(f),
        }
    }
}

impl PartialEq<usize> for RetVal {
    fn eq(&self, other: &usize) -> bool {
        matches!(self, Self::IntRet(n) if n == other)
    }
}

impl PartialEq<&str> for RetVal {
    fn eq(&self, other: &&str) -> bool {
        matches!(self, Self::StrRet(s) if s == other)
    }
}

pub type Return = Result<RetVal>;

pub fn ret<T: Into<RetVal>>(t: T) -> Return {
    Ok(t.into())
}
