use std::str::FromStr;
use utils::types::*;

#[derive(Clone, Copy)]
enum Register {
    W,
    X,
    Y,
    Z,
}

impl FromStr for Register {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        Ok(match s {
            "w" => Self::W,
            "x" => Self::X,
            "y" => Self::Y,
            "z" => Self::Z,
            _ => return Err(Error::Parsing(s.to_string())),
        })
    }
}

enum Argument {
    Imm(isize),
    Reg(Register),
}

impl FromStr for Argument {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        if let Ok(n) = s.parse::<isize>() {
            Ok(Self::Imm(n))
        } else if let Ok(r) = s.parse::<Register>() {
            Ok(Self::Reg(r))
        } else {
            Err(Error::Parsing(s.to_string()))
        }
    }
}

enum Instruction {
    Inp(Register),
    Add(Register, Argument),
    Mul(Register, Argument),
    Div(Register, Argument),
    Mod(Register, Argument),
    Eql(Register, Argument),
    Dif(Register, Argument),
}

impl FromStr for Instruction {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let split: Vec<_> = s.split_whitespace().collect();
        match split.len() {
            2 => {
                let reg = split[1].parse()?;
                if split[0] == "inp" {
                    Ok(Self::Inp(reg))
                } else {
                    Err(Error::Parsing(s.to_string()))
                }
            }
            3 => {
                let reg = split[1].parse()?;
                let arg = split[2].parse()?;
                match split[0] {
                    "add" => Ok(Self::Add(reg, arg)),
                    "mul" => Ok(Self::Mul(reg, arg)),
                    "div" => Ok(Self::Div(reg, arg)),
                    "mod" => Ok(Self::Mod(reg, arg)),
                    "eql" => Ok(Self::Eql(reg, arg)),
                    "dif" => Ok(Self::Dif(reg, arg)),
                    _ => Err(Error::Parsing(s.to_string())),
                }
            }
            _ => Err(Error::Parsing(s.to_string())),
        }
    }
}

#[derive(Clone, PartialEq)]
enum Expression {
    Imm(isize),
    Var(String),
    Add(Box<Expression>, Box<Expression>),
    Mul(Box<Expression>, Box<Expression>),
    Div(Box<Expression>, Box<Expression>),
    Mod(Box<Expression>, Box<Expression>),
    Eql(Box<Expression>, Box<Expression>),
    Dif(Box<Expression>, Box<Expression>),
    Ite(Box<Expression>, Box<Expression>, Box<Expression>),
}

mod expression_traits {
    use super::*;
    use std::fmt;
    use std::ops::{Add, Div, Mul, Rem};

    impl fmt::Display for Expression {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Self::Imm(n) => n.fmt(f),
                Self::Var(v) => v.fmt(f),
                Self::Add(e1, e2) => match **e2 {
                    Self::Imm(n) if n < 0 => write!(f, "({} - {})", e1, -n),
                    _ => write!(f, "({} + {})", e1, e2),
                },
                Self::Mul(e1, e2) => write!(f, "({} * {})", e1, e2),
                Self::Div(e1, e2) => write!(f, "({} / {})", e1, e2),
                Self::Mod(e1, e2) => write!(f, "({} % {})", e1, e2),
                Self::Eql(e1, e2) => write!(f, "({} == {})", e1, e2),
                Self::Dif(e1, e2) => write!(f, "({} != {})", e1, e2),
                Self::Ite(e1, e2, e3) => write!(f, "if {} then\n  {}\nelse\n  {}", e1, e2, e3),
            }
        }
    }

    impl From<isize> for Box<Expression> {
        fn from(i: isize) -> Self {
            Box::new(Expression::Imm(i))
        }
    }

    impl From<String> for Box<Expression> {
        fn from(s: String) -> Self {
            Box::new(Expression::Var(s))
        }
    }

    impl Add<isize> for Expression {
        type Output = Self;

        fn add(self, rhs: isize) -> Self::Output {
            if rhs == 0 {
                return self;
            }
            match self {
                Self::Imm(i) => Self::Imm(i + rhs),
                Self::Add(l, r) if matches!(*r, Self::Imm(_)) => Self::Add(l, (*r + rhs).into()),
                Self::Ite(c, t, e) => Self::Ite(c, (*t + rhs).into(), (*e + rhs).into()),
                _ => Self::Add(self.into(), rhs.into()),
            }
        }
    }

    impl Add for Expression {
        type Output = Self;

        fn add(self, other: Self) -> Self::Output {
            match (self, other) {
                (l, Self::Imm(i)) => l + i,
                (Self::Imm(i), r) => r + i,
                (expr, Self::Ite(c, t, e)) | (Self::Ite(c, t, e), expr) => {
                    Self::Ite(c, (expr.clone() + *t).into(), (expr + *e).into())
                }
                (l, r) => Self::Add(l.into(), r.into()),
            }
        }
    }

    impl Mul<isize> for Expression {
        type Output = Self;

        fn mul(self, rhs: isize) -> Self::Output {
            match (self, rhs) {
                (_, 0) => Self::Imm(0),
                (e, 1) => e,
                (Self::Imm(i), n) => Self::Imm(i * n),
                (e, n) => Self::Mul(e.into(), n.into()),
            }
        }
    }

    impl Mul for Expression {
        type Output = Self;

        fn mul(self, other: Self) -> Self::Output {
            match (self, other) {
                (l, Self::Imm(i)) => l * i,
                (Self::Imm(i), r) => r * i,
                (expr, Self::Dif(dl, dr)) | (Self::Dif(dl, dr), expr) => {
                    Self::Ite(Self::Dif(dl, dr).into(), expr.into(), 0.into())
                }
                (expr, Self::Ite(c, t, e)) | (Self::Ite(c, t, e), expr) => {
                    Self::Ite(c, (expr.clone() * *t).into(), (expr * *e).into())
                }
                (l, r) => Self::Mul(l.into(), r.into()),
            }
        }
    }

    impl Div<isize> for Expression {
        type Output = Self;

        fn div(self, rhs: isize) -> Self::Output {
            if rhs == 1 {
                return self;
            }
            match &self {
                Self::Imm(i) => Self::Imm(i / rhs),
                Self::Add(l, r) => (*l.clone() / rhs) + (*r.clone() / rhs),
                Self::Mul(l, r) => match (&**l, &**r) {
                    (x, Self::Imm(n)) | (Self::Imm(n), x) if *n == rhs => x.clone(),
                    _ => Self::Div(self.into(), rhs.into()),
                },
                Self::Ite(c, t, e) => Self::Ite(
                    c.clone(),
                    (*t.clone() / rhs).into(),
                    (*e.clone() / rhs).into(),
                ),
                _ => Self::Div(self.into(), rhs.into()),
            }
        }
    }

    impl Div for Expression {
        type Output = Self;

        fn div(self, other: Self) -> Self::Output {
            match (&self, &other) {
                (_, Self::Imm(i)) => self / *i,
                (Self::Add(ll, lr), r) => (*ll.clone() / r.clone()) + (*lr.clone() / r.clone()),
                (Self::Ite(c, t, e), n) => Self::Ite(
                    c.clone(),
                    (*t.clone() / n.clone()).into(),
                    (*e.clone() / n.clone()).into(),
                ),
                _ => Self::Dif(self.into(), other.into()),
            }
        }
    }

    impl Rem<isize> for Expression {
        type Output = Self;

        fn rem(self, rhs: isize) -> Self::Output {
            match self {
                Self::Imm(i) => Self::Imm(i % rhs),
                Self::Ite(c, t, e) => Self::Ite(c, (*t % rhs).into(), (*e % rhs).into()),
                _ => Self::Mod(self.into(), rhs.into()),
            }
        }
    }

    impl Rem for Expression {
        type Output = Self;

        fn rem(self, other: Self) -> Self::Output {
            match (self, other) {
                (l, Self::Imm(i)) => l % i,
                (Self::Ite(c, t, e), n) => Self::Ite(c, (*t % n.clone()).into(), (*e % n).into()),
                (e1, e2) => Self::Mod(e1.into(), e2.into()),
            }
        }
    }
}

impl Expression {
    fn reduce(&self) -> Self {
        match self {
            Self::Imm(_) | Self::Var(_) => self.clone(),
            Self::Add(e1, e2) => e1.reduce() + e2.reduce(),
            Self::Mul(e1, e2) => e1.reduce() * e2.reduce(),
            Self::Div(e1, e2) => e1.reduce() / e2.reduce(),
            Self::Mod(e1, e2) => e1.reduce() % e2.reduce(),
            Self::Eql(e1, e2) => match (e1.reduce(), e2.reduce()) {
                (Self::Eql(l1, l2), Self::Imm(0)) => Self::Dif(l1, l2),
                (e1, e2) => Self::Eql(e1.into(), e2.into()),
            },
            Self::Dif(e1, e2) => match (e1.reduce(), e2.reduce()) {
                (Self::Imm(i1), Self::Imm(i2)) => Self::Imm((i1 != i2).into()),
                (Self::Var(_), expr) | (expr, Self::Var(_)) if expr.min_value() > 9 => Self::Imm(1),
                (e1, e2) => Self::Dif(e1.into(), e2.into()),
            },
            Self::Ite(e1, e2, e3) => match e1.reduce() {
                Self::Imm(0) => e3.reduce(),
                Self::Imm(1) => e2.reduce(),
                e1 => Self::Ite(e1.into(), e2.reduce().into(), e3.reduce().into()),
            },
        }
    }

    fn min_value(&self) -> isize {
        match self {
            Self::Imm(n) => *n,
            Self::Var(_) => 1,
            Self::Add(l, r) => l.min_value() + r.min_value(),
            Self::Mul(l, r) => l.min_value() * r.min_value(),
            Self::Div(l, r) => l.min_value() / r.min_value(),
            Self::Mod(l, r) => l.min_value() % r.min_value(),
            Self::Eql(_, _) => 0,
            Self::Dif(_, _) => 0,
            Self::Ite(_, t, e) => t.min_value().min(e.min_value()),
        }
    }
}

struct State {
    registers: [Expression; 4],
    next_input: u8,
}

mod state_traits {
    use super::*;
    use std::fmt;
    use std::ops::{Index, IndexMut};

    impl fmt::Display for State {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(
                f,
                "{{\ntw: {},\ntx: {},\nty: {},\ntz: {},\ntnext_input: {}\n}}",
                self.registers[0],
                self.registers[1],
                self.registers[2],
                self.registers[3],
                self.next_input
            )
        }
    }

    impl Index<Register> for State {
        type Output = Expression;

        fn index(&self, r: Register) -> &Self::Output {
            match r {
                Register::W => &self.registers[0],
                Register::X => &self.registers[1],
                Register::Y => &self.registers[2],
                Register::Z => &self.registers[3],
            }
        }
    }

    impl IndexMut<Register> for State {
        fn index_mut(&mut self, r: Register) -> &mut Self::Output {
            match r {
                Register::W => &mut self.registers[0],
                Register::X => &mut self.registers[1],
                Register::Y => &mut self.registers[2],
                Register::Z => &mut self.registers[3],
            }
        }
    }
}

impl State {
    fn exec(&mut self, instr: Instruction) {
        match instr {
            Instruction::Inp(r) => {
                let v = Expression::Var(format!("${}", self.next_input));
                self.next_input += 1;
                self[r] = v;
            }
            Instruction::Add(r, Argument::Imm(n)) => self[r] = self[r].clone() + n,
            Instruction::Add(r1, Argument::Reg(r2)) => {
                self[r1] = self[r1].clone() + self[r2].clone()
            }

            Instruction::Mul(r, Argument::Imm(n)) => self[r] = self[r].clone() * n,
            Instruction::Mul(r1, Argument::Reg(r2)) => {
                self[r1] = self[r1].clone() * self[r2].clone()
            }

            Instruction::Div(r, Argument::Imm(n)) => self[r] = self[r].clone() / n,
            Instruction::Div(r1, Argument::Reg(r2)) => {
                self[r1] = self[r1].clone() / self[r2].clone()
            }

            Instruction::Mod(r, Argument::Imm(n)) => self[r] = self[r].clone() % n,
            Instruction::Mod(r1, Argument::Reg(r2)) => {
                self[r1] = self[r1].clone() % self[r2].clone()
            }

            Instruction::Eql(r, Argument::Imm(n)) => {
                self[r] = Expression::Eql(self[r].clone().into(), n.into())
            }
            Instruction::Eql(r1, Argument::Reg(r2)) => {
                self[r1] = Expression::Eql(self[r1].clone().into(), self[r2].clone().into())
            }
            Instruction::Dif(r, Argument::Imm(n)) => {
                self[r] = Expression::Dif(self[r].clone().into(), n.into())
            }
            Instruction::Dif(r1, Argument::Reg(r2)) => {
                self[r1] = Expression::Dif(self[r1].clone().into(), self[r2].clone().into())
            }
        }
    }

    fn reduce_z(&mut self) {
        loop {
            let old_z = self.registers[3].clone();
            self.registers[3] = old_z.reduce();
            if old_z == self.registers[3] {
                break;
            }
        }
    }

    fn block_default(n: u8) -> Self {
        Self {
            registers: if n == 0 {
                [
                    Expression::Imm(0),
                    Expression::Imm(0),
                    Expression::Imm(0),
                    Expression::Imm(0),
                ]
            } else {
                [
                    Expression::Var(format!("w_{}", n - 1)),
                    Expression::Var(format!("x_{}", n - 1)),
                    Expression::Var(format!("y_{}", n - 1)),
                    Expression::Var(format!("z_{}", n - 1)),
                ]
            },
            next_input: n,
        }
    }
}

fn solve(equations: &[Expression]) -> Result<usize> {
    let mut answers = [0; 14];
    for eq in equations {
        if let Expression::Eql(l, r) = eq {
            match (&**l, &**r) {
                (Expression::Var(v1), Expression::Var(v2)) => {
                    let n1 = v1[1..].parse::<usize>()?;
                    let n2 = v2[1..].parse::<usize>()?;
                    answers[n1] = 1;
                    answers[n2] = 1;
                    continue;
                }
                (Expression::Add(ll, lr), Expression::Var(v2)) => {
                    if let (Expression::Var(v1), Expression::Imm(n)) = (&**ll, &**lr) {
                        let n1 = v1[1..].parse::<usize>()?;
                        let n2 = v2[1..].parse::<usize>()?;
                        if *n > 0 {
                            answers[n1] = 1;
                            answers[n2] = 1 + *n as usize;
                        } else {
                            answers[n1] = (1 - *n) as usize;
                            answers[n2] = 1;
                        }
                        continue;
                    }
                }
                _ => (),
            }
        }
        return Err(unreachable(&format!(
            "An equation should be of the form $x (± y)? == $z instead of {}",
            eq
        )));
    }
    let mut res = 0;
    for (i, answer) in answers.into_iter().enumerate() {
        res += answer * 10usize.pow(13 - i as u32);
    }
    Ok(res)
}

const BLOCK_SIZE: usize = 18;

pub fn day(contents: String) -> Return {
    let mut blocks = Vec::new();
    for (i, line) in contents.lines().enumerate() {
        if i % BLOCK_SIZE == 0 {
            blocks.push(Vec::new());
        }
        blocks[i / BLOCK_SIZE].push(line.parse::<Instruction>()?);
    }
    let mut modulos = Vec::new();
    let mut equations = Vec::new();
    for (n, block) in blocks.into_iter().enumerate() {
        let mut state = State::block_default(n as u8);
        for instr in block {
            state.exec(instr);
        }
        state.reduce_z();
        match &state.registers[3] {
            Expression::Add(l, r) if matches!(**l, Expression::Mul(_, _)) => {
                modulos.push(*r.clone());
                continue;
            }
            z @ Expression::Add(_, _) => {
                modulos.push(z.clone());
                continue;
            }
            Expression::Ite(c, _, _) => {
                if let Expression::Dif(l, r) = &**c {
                    if let Expression::Add(_z_mod_26, x) = &**l {
                        let modulo = modulos.pop().ok_or_else(|| {
                            unreachable("Every if-then-else should have its associated modulo")
                        })?;
                        let equation = Expression::Eql((modulo + *x.clone()).into(), r.clone());
                        equations.push(equation);
                        continue;
                    }
                }
            }
            _ => (),
        }
        return Err(unreachable(&format!(
            "After reduction, z value should be of either form: (z_prev * 26)? + ($x + y) OR if (z_prev % 26) ± x != $y then ... else ..., instead I got: {}", state.registers[3]
        )));
    }
    if !modulos.is_empty() {
        return Err(unreachable("Unused modulo leftover"));
    }
    ret(solve(&equations)?)
}

#[cfg(test)]
mod day24 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 21611513911181);
        Ok(())
    }
}
