use std::str::FromStr;
use utils::types::*;

enum Command {
    Forward(usize),
    Down(usize),
    Up(usize),
}

impl FromStr for Command {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let (direction, n) = s
            .split_once(' ')
            .ok_or_else(|| Error::Parsing(s.to_string()))?;
        let n = n.parse::<usize>()?;
        match direction {
            "forward" => Ok(Self::Forward(n)),
            "down" => Ok(Self::Down(n)),
            "up" => Ok(Self::Up(n)),
            _ => Err(Error::Parsing(s.to_string())),
        }
    }
}

pub fn day(contents: String) -> Return {
    let mut pos = 0;
    let mut aim = 0;
    let mut depth = 0;
    for s in contents.lines() {
        match s.parse()? {
            Command::Forward(n) => {
                pos += n;
                depth += aim * n
            }
            Command::Down(n) => {
                aim += n;
            }
            Command::Up(n) => aim -= n,
        }
    }
    ret(pos * depth)
}

#[cfg(test)]
mod day2 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 900);
        Ok(())
    }
}
