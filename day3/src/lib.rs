use utils::types::*;

fn aux(report: &[usize], mut i: usize, most: bool) -> Result<usize> {
    let mut search_space = report.to_vec();
    while search_space.len() > 1 {
        let (ones, zeroes): (Vec<_>, Vec<_>) = search_space.iter().partition(|&n| n & i == i);
        let (bigger, smaller) = if ones.len() >= zeroes.len() {
            (ones, zeroes)
        } else {
            (zeroes, ones)
        };
        search_space = if most { bigger } else { smaller };
        i >>= 1;
    }

    Ok(search_space[0])
}

pub fn day(contents: String) -> Return {
    let size = 1
        << (contents
            .find('\n')
            .ok_or_else(|| Error::Parsing(contents.to_string()))?
            - 1);
    let report = contents
        .lines()
        .map(|line| Ok(usize::from_str_radix(line, 2)?))
        .collect::<Result<Vec<_>>>()?;
    let o2 = aux(&report, size, true)?;
    let co2 = aux(&report, size, false)?;
    ret(o2 * co2)
}

#[cfg(test)]
mod day3 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 230);
        Ok(())
    }
}
