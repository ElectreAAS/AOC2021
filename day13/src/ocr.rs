use utils::types::*;

/// Supported letters: ABCEFGHJKLPRUZ
const LETTERS: [(char, [[bool; 6]; 4]); 14] = [
    (
        'A',
        [
            [false, true, true, true, true, true],
            [true, false, false, true, false, false],
            [true, false, false, true, false, false],
            [false, true, true, true, true, true],
        ],
    ),
    (
        'B',
        [
            [true; 6],
            [true, false, true, false, false, true],
            [true, false, true, false, false, true],
            [false, true, false, true, true, false],
        ],
    ),
    (
        'C',
        [
            [false, true, true, true, true, false],
            [true, false, false, false, false, true],
            [true, false, false, false, false, true],
            [false, true, false, false, true, false],
        ],
    ),
    (
        'E',
        [
            [true; 6],
            [true, false, true, false, false, true],
            [true, false, true, false, false, true],
            [true, false, false, false, false, true],
        ],
    ),
    (
        'F',
        [
            [true; 6],
            [true, false, true, false, false, false],
            [true, false, true, false, false, false],
            [true, false, false, false, false, false],
        ],
    ),
    (
        'G',
        [
            [false, true, true, true, true, false],
            [true, false, false, false, false, true],
            [true, false, false, true, false, true],
            [false, true, false, true, true, true],
        ],
    ),
    (
        'H',
        [
            [true; 6],
            [false, false, true, false, false, false],
            [false, false, true, false, false, false],
            [true; 6],
        ],
    ),
    (
        'J',
        [
            [false, false, false, false, true, false],
            [false, false, false, false, false, true],
            [true, false, false, false, false, true],
            [true, true, true, true, true, false],
        ],
    ),
    (
        'K',
        [
            [true; 6],
            [false, false, true, false, false, false],
            [false, true, false, true, true, false],
            [true, false, false, false, false, true],
        ],
    ),
    (
        'L',
        [
            [true; 6],
            [false, false, false, false, false, true],
            [false, false, false, false, false, true],
            [false, false, false, false, false, true],
        ],
    ),
    (
        'P',
        [
            [true; 6],
            [true, false, false, true, false, false],
            [true, false, false, true, false, false],
            [false, true, true, false, false, false],
        ],
    ),
    (
        'R',
        [
            [true; 6],
            [true, false, false, true, false, false],
            [true, false, false, true, true, false],
            [false, true, true, false, false, true],
        ],
    ),
    (
        'U',
        [
            [true, true, true, true, true, false],
            [false, false, false, false, false, true],
            [false, false, false, false, false, true],
            [true, true, true, true, true, false],
        ],
    ),
    (
        'Z',
        [
            [true, false, false, false, true, true],
            [true, false, false, true, false, true],
            [true, false, true, false, false, true],
            [true, true, false, false, false, true],
        ],
    ),
];

pub fn recog(chunk: &[&bool]) -> Result<char> {
    for (ch, letter) in LETTERS {
        if letter
            .iter()
            .flatten()
            .enumerate()
            .all(|(n, b)| b == chunk[n])
        {
            return Ok(ch);
        }
    }
    Err(unreachable("Unknown letter"))
}
