#!/bin/fish
set cookie $AOC2021_SESSION

if test -n "$cookie"
    for i in $argv
        set dir_name "./day$i"
        if test ! -d $dir_name
            mkdir $dir_name
        end
        set url 'https://adventofcode.com/2021/day/'$i'/input'
        set file_name "$dir_name/input.txt"
        if test ! -f $file_name
            echo "Creating file $file_name"
            curl "$url" -X GET -H "Cookie: session=$cookie" >$file_name
        else
            echo "File $file_name already exists, no action needed."
        end
    end
else
    echo "Session cookie not found!"
end
