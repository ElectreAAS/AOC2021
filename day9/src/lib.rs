use std::fmt;
use std::str::FromStr;
use utils::types::*;

struct Case {
    num: u8,
    basin: Option<usize>,
}

impl TryFrom<char> for Case {
    type Error = Error;

    fn try_from(ch: char) -> Result<Self> {
        Ok(Case {
            num: ch.to_string().parse()?,
            basin: None,
        })
    }
}

struct HeatMap {
    grid: Vec<Vec<Case>>,
    basin_sizes: Vec<usize>,
}

impl FromStr for HeatMap {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let len = s.find('\n').ok_or_else(|| Error::Parsing(s.to_string()))?;
        let mut grid = Vec::with_capacity(len);
        for (y, line) in s.lines().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                if y == 0 {
                    grid.push(Vec::new());
                }
                grid[x].push(ch.try_into()?);
            }
        }
        Ok(HeatMap {
            grid,
            basin_sizes: vec![],
        })
    }
}

impl fmt::Display for HeatMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let v: Vec<Vec<_>> = self
            .grid
            .iter()
            .map(|line| {
                line.iter()
                    .map(|c| match c.basin {
                        None => String::from(" "),
                        Some(b) => b.to_string(),
                    })
                    .collect()
            })
            .collect();
        utils::box_format(&v).fmt(f)
    }
}

fn expand_basin(x: usize, y: usize, hm: &mut HeatMap) -> Result<usize> {
    let basin = hm.grid[x][y]
        .basin
        .ok_or_else(|| unreachable("Starting case should already have a basin"))?;
    // Recurse top
    let top = if y > 0 {
        let top_c = &mut hm.grid[x][y - 1];
        if top_c.num != 9 && top_c.basin.is_none() {
            top_c.basin = Some(basin);
            expand_basin(x, y - 1, hm)?
        } else {
            0
        }
    } else {
        0
    };
    // Recurse right
    let right = if let Some(right_col) = hm.grid.get_mut(x + 1) {
        let right_c = &mut right_col[y];
        if right_c.num != 9 && right_c.basin.is_none() {
            right_c.basin = Some(basin);
            expand_basin(x + 1, y, hm)?
        } else {
            0
        }
    } else {
        0
    };
    // Recurse bot
    let bot = if let Some(bot_c) = hm.grid[x].get_mut(y + 1) {
        if bot_c.num != 9 && bot_c.basin.is_none() {
            bot_c.basin = Some(basin);
            expand_basin(x, y + 1, hm)?
        } else {
            0
        }
    } else {
        0
    };
    // Recurse left
    let left = if x > 0 {
        let left_c = &mut hm.grid[x - 1][y];
        if left_c.num != 9 && left_c.basin.is_none() {
            left_c.basin = Some(basin);
            expand_basin(x - 1, y, hm)?
        } else {
            0
        }
    } else {
        0
    };
    Ok(top + right + bot + left + 1)
}

pub fn day(contents: String) -> Return {
    let mut hm: HeatMap = contents.parse()?;
    let width = hm.grid.len();
    let height = hm.grid[0].len();
    for x in 0..width {
        for y in 0..height {
            let this = &mut hm.grid[x][y];
            if this.num == 9 || this.basin.is_some() {
                continue;
            }
            let basin = hm.basin_sizes.len();
            this.basin = Some(basin);

            let right = if x < width - 1 {
                let right_c = &mut hm.grid[x + 1][y];
                if right_c.num != 9 {
                    right_c.basin = Some(basin);
                    expand_basin(x + 1, y, &mut hm)?
                } else {
                    0
                }
            } else {
                0
            };
            let bot = if y < height - 1 {
                let bot_c = &mut hm.grid[x][y + 1];
                if bot_c.num != 9 && bot_c.basin.is_none() {
                    bot_c.basin = Some(basin);
                    expand_basin(x, y + 1, &mut hm)?
                } else {
                    0
                }
            } else {
                0
            };
            hm.basin_sizes.push(right + bot + 1);
        }
    }
    hm.basin_sizes.sort_unstable();
    ret::<usize>(hm.basin_sizes[hm.basin_sizes.len() - 3..].iter().product())
}

#[cfg(test)]
mod day9 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 1134);
        Ok(())
    }
}
