use std::collections::VecDeque;
use std::fmt;
use std::ops::{Deref, DerefMut};
use std::str::FromStr;
use utils::types::*;

const ENHANCEMENTS: usize = 50;

struct Algo([bool; 512]);

impl FromStr for Algo {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut array = [false; 512];
        for (i, ch) in s.chars().enumerate() {
            if ch == '#' {
                array[i] = true;
            }
        }
        Ok(Algo(array))
    }
}

impl Deref for Algo {
    type Target = [bool; 512];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Clone)]
struct Image(VecDeque<VecDeque<bool>>);

impl Deref for Image {
    type Target = VecDeque<VecDeque<bool>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Image {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl FromStr for Image {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let width = s.find('\n').ok_or_else(|| Error::Parsing(s.to_string()))?;
        let mut v = VecDeque::from(vec![VecDeque::from(vec![false; width]); width]);
        for (y, line) in s.lines().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                if ch == '#' {
                    v[x][y] = true;
                }
            }
        }

        Ok(Image(v))
    }
}

impl fmt::Display for Image {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();
        for y in 0..self.len() {
            for x in 0..self.len() {
                if self[x][y] {
                    s.push('█');
                } else {
                    s.push(' ');
                }
            }
            s.push('\n');
        }
        s.fmt(f)
    }
}

impl Image {
    fn enlarge(&mut self) {
        let size = self.len();
        for col in self.iter_mut() {
            col.push_front(false);
            col.push_back(false);
        }
        self.push_front(VecDeque::from(vec![false; size + 2]));
        self.push_back(VecDeque::from(vec![false; size + 2]));
    }

    fn enhance(mut self, algo: &Algo) -> usize {
        let mut size = self.len();
        let mut other = self.clone();
        for step in 0..ENHANCEMENTS {
            self.enlarge();
            other.enlarge();
            let (idle, target) = if step % 2 == 0 {
                (&self, &mut other)
            } else {
                (&other, &mut self)
            };
            let outside_lit = algo[0] && step % 2 == 1;
            size += 2;

            for y in 0..size {
                for x in 0..size {
                    let mut n = 0;
                    for j in 0..3 {
                        for i in 0..3 {
                            let xi = x + i;
                            let yj = y + j;
                            n <<= 1;
                            let neigh = if xi <= 1 || xi >= size || yj <= 1 || yj >= size {
                                outside_lit
                            } else {
                                idle[xi - 1][yj - 1]
                            };

                            if neigh {
                                n += 1;
                            }
                        }
                    }
                    target[x][y] = algo[n];
                }
            }
        }
        self.popcount()
    }

    fn popcount(&self) -> usize {
        self.iter().fold(0, |sum, col| {
            col.iter().fold(0, |sum, b| if *b { sum + 1 } else { sum }) + sum
        })
    }
}

pub fn day(contents: String) -> Return {
    let (algo, rest) = contents
        .split_once("\n\n")
        .ok_or_else(|| Error::Parsing(contents.to_string()))?;
    let algo = algo.parse()?;
    let image: Image = rest.parse()?;
    ret(image.enhance(&algo))
}

#[cfg(test)]
mod day20 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 3351);
        Ok(())
    }
}
