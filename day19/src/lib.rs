use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt;
use std::str::FromStr;
use utils::types::*;

type Matrix = [[isize; 3]; 3];

const ROTATION_MATRICES: [Matrix; 24] = [
    [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    [[1, 0, 0], [0, 0, 1], [0, -1, 0]],
    [[1, 0, 0], [0, -1, 0], [0, 0, -1]],
    [[1, 0, 0], [0, 0, -1], [0, 1, 0]],
    [[0, 1, 0], [0, 0, 1], [1, 0, 0]],
    [[0, 1, 0], [1, 0, 0], [0, 0, -1]],
    [[0, 1, 0], [0, 0, -1], [-1, 0, 0]],
    [[0, 1, 0], [-1, 0, 0], [0, 0, 1]],
    [[0, 0, 1], [1, 0, 0], [0, 1, 0]],
    [[0, 0, 1], [0, 1, 0], [-1, 0, 0]],
    [[0, 0, 1], [-1, 0, 0], [0, -1, 0]],
    [[0, 0, 1], [0, -1, 0], [1, 0, 0]],
    [[-1, 0, 0], [0, -1, 0], [0, 0, 1]],
    [[-1, 0, 0], [0, 0, 1], [0, 1, 0]],
    [[-1, 0, 0], [0, 1, 0], [0, 0, -1]],
    [[-1, 0, 0], [0, 0, -1], [0, -1, 0]],
    [[0, -1, 0], [0, 0, -1], [1, 0, 0]],
    [[0, -1, 0], [1, 0, 0], [0, 0, 1]],
    [[0, -1, 0], [0, 0, 1], [-1, 0, 0]],
    [[0, -1, 0], [-1, 0, 0], [0, 0, -1]],
    [[0, 0, -1], [-1, 0, 0], [0, 1, 0]],
    [[0, 0, -1], [0, 1, 0], [1, 0, 0]],
    [[0, 0, -1], [1, 0, 0], [0, -1, 0]],
    [[0, 0, -1], [0, -1, 0], [-1, 0, 0]],
];

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
struct Beacon {
    x: isize,
    y: isize,
    z: isize,
}

impl FromStr for Beacon {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let nums = s
            .split(',')
            .map(|n| Ok(n.parse::<isize>()?))
            .collect::<Result<Vec<_>>>()?;
        if nums.len() != 3 {
            return Err(Error::Parsing(s.to_string()));
        }
        Ok(Beacon {
            x: nums[0],
            y: nums[1],
            z: nums[2],
        })
    }
}

impl fmt::Display for Beacon {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{},{},{}", self.x, self.y, self.z)
    }
}

impl Beacon {
    /// Compute the difference in positions between self and other.
    fn offset_with(&self, other: &Self) -> (isize, isize, isize) {
        (self.x - other.x, self.y - other.y, self.z - other.z)
    }

    /// Move self by provided offset.
    fn offset_by(&mut self, offset: (isize, isize, isize)) {
        self.x += offset.0;
        self.y += offset.1;
        self.z += offset.2;
    }

    fn squared_distance(&self, other: &Self) -> usize {
        ((self.x - other.x).pow(2) + (self.y - other.y).pow(2) + (self.z - other.z).pow(2)) as usize
    }

    fn rotate(&self, m: &Matrix) -> Self {
        Beacon {
            x: m[0][0] * self.x + m[0][1] * self.y + m[0][2] * self.z,
            y: m[1][0] * self.x + m[1][1] * self.y + m[1][2] * self.z,
            z: m[2][0] * self.x + m[2][1] * self.y + m[2][2] * self.z,
        }
    }
}

struct Scanner {
    id: usize,
    position: Beacon,
    distances: HashSet<usize>,
    beacons: Vec<Beacon>,
}

impl FromStr for Scanner {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let v: Vec<_> = s
            .split_whitespace()
            .filter_map(|s| s.parse().ok())
            .collect();
        let id = *v.get(0).ok_or_else(|| Error::Parsing(s.to_string()))?;
        Ok(Scanner {
            id,
            position: Beacon { x: 0, y: 0, z: 0 },
            distances: HashSet::new(),
            beacons: vec![],
        })
    }
}

impl fmt::Display for Scanner {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = format!("─── scanner {} ─── @ {}\n", self.id, self.position);
        for b in self.beacons.iter() {
            s.push_str(&format!("{}\n", b));
        }
        writeln!(f, "{}", s)
    }
}

impl Scanner {
    fn compute_distances(&mut self) {
        let len = self.beacons.len();
        let mut s = HashSet::with_capacity(len.pow(2));
        for i in 0..len - 1 {
            for j in i + 1..len {
                s.insert(self.beacons[i].squared_distance(&self.beacons[j]));
            }
        }
        self.distances = s;
    }

    fn rotate(&self, m: &Matrix) -> Self {
        Self {
            id: self.id,
            position: self.position,
            distances: self.distances.clone(),
            beacons: self.beacons.iter().map(|b| b.rotate(m)).collect(),
        }
    }

    fn offset_by(&mut self, offset: (isize, isize, isize)) {
        self.position.offset_by(offset);
        for b in self.beacons.iter_mut() {
            b.offset_by(offset);
        }
    }
}

fn parse_all(contents: String) -> Result<Vec<Scanner>> {
    let mut v: Vec<Scanner> = Vec::new();
    let mut i = 0;
    for line in contents.lines() {
        if line.is_empty() {
            i += 1;
            continue;
        }
        if let Ok(b) = line.parse::<Beacon>() {
            v[i].beacons.push(b);
        } else {
            v.push(line.parse::<Scanner>()?);
        }
    }
    for s in v.iter_mut() {
        s.compute_distances();
    }
    Ok(v)
}

pub fn day(contents: String) -> Return {
    let mut scanners = VecDeque::from(parse_all(contents)?);
    let mut true_scans = Vec::new();
    while let Some(scanner) = scanners.pop_front() {
        // At first we establish the first scanner as our truth source:
        // its position is 0,0,0 and orientation is normal.
        if true_scans.is_empty() {
            true_scans.push(scanner);
            continue;
        }

        let mut matched_scanner = None;
        'references: for truth in true_scans.iter() {
            // 12 common beacons means 12*11/2 = 66 common distances
            if truth.distances.intersection(&scanner.distances).count() >= 66 {
                // We have a probable match, we have to try all rotations
                for rot in ROTATION_MATRICES {
                    let mut s = scanner.rotate(&rot);
                    let mut possible_offsets = HashMap::new();
                    let mut good_offset = None;
                    'outer: for b1 in s.beacons.iter() {
                        for b2 in truth.beacons.iter() {
                            let offset = b2.offset_with(b1);
                            let e = possible_offsets.entry(offset).or_insert(0);
                            *e += 1;
                            if *e == 12 {
                                good_offset = Some(offset);
                                break 'outer;
                            }
                        }
                    }
                    if let Some(offset) = good_offset {
                        // We have found the correct offset for this scanner,
                        // we apply it and mark it as a truth source.
                        s.offset_by(offset);
                        matched_scanner = Some(s);
                        break 'references;
                    }
                }
            }
        }
        if let Some(s) = matched_scanner {
            true_scans.push(s);
        } else {
            // In case we haven't found the correct offset + rotation for scanner,
            // we add it to the back of the todo list.
            // This doesn't create an infinite loop in case of lonely scanner,
            // since the puzzle text clearly states that scanners map a contiguous zone.
            scanners.push_back(scanner);
        }
    }
    let mut max_dist = 0;
    for i in 0..true_scans.len() - 1 {
        for j in i + 1..true_scans.len() {
            let (x, y, z) = true_scans[i].position.offset_with(&true_scans[j].position);
            let dist = x.abs() + y.abs() + z.abs();
            max_dist = max_dist.max(dist as usize);
        }
    }
    ret(max_dist)
}

#[cfg(test)]
mod day19 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 3621);
        Ok(())
    }
}
