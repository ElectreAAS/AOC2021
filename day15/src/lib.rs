use utils::types::*;

use std::fmt;

/// An expanded position is an association between:
/// - the (x, y) coordinates of the new case
/// - the offset `d_num` to be added to the num in the case
/// - the heuristic distance between that case and the end
#[derive(Clone, Copy)]
struct ExpandedPos {
    x: usize,
    y: usize,
    d_num: usize,
    heuristic_dist: usize,
}

impl ExpandedPos {
    const fn default_const() -> Self {
        ExpandedPos {
            x: 0,
            y: 0,
            d_num: 0,
            heuristic_dist: 0,
        }
    }
}

/// EXPANSIONS[x][y] is a list of the 25 positions linked to
/// position (x, y) in the un-expanded grid.
///
/// In test config, the map is only 10x10
#[cfg(test)]
static EXPANSIONS: [[[ExpandedPos; 25]; 10]; 10] = {
    let mut offsets = [[[ExpandedPos::default_const(); 25]; 10]; 10];
    let mut x = 0;
    while x < 10 {
        let mut y = 0;
        while y < 10 {
            let mut row = 0;
            while row < 5 {
                let mut col = 0;
                while col < 5 {
                    offsets[x][y][5 * row + col] = ExpandedPos {
                        x: 10 * col + x,
                        y: 10 * row + y,
                        d_num: row + col,
                        heuristic_dist: 98 - (10 * (row + col)) - x - y,
                    };
                    col += 1;
                }
                row += 1;
            }
            y += 1;
        }
        x += 1;
    }
    offsets
};

/// EXPANSIONS[x][y] is a list of the 25 positions linked to
/// position (x, y) in the un-expanded grid.
///
/// On real data, the map is 100x100
#[cfg(not(test))]
static EXPANSIONS: [[[ExpandedPos; 25]; 100]; 100] = {
    let mut offsets = [[[ExpandedPos::default_const(); 25]; 100]; 100];
    let mut x = 0;
    while x < 100 {
        let mut y = 0;
        while y < 100 {
            offsets[x][y] = [
                // First row
                ExpandedPos {
                    x,
                    y,
                    d_num: 0,
                    heuristic_dist: 998 - x - y,
                },
                ExpandedPos {
                    x: 100 + x,
                    y,
                    d_num: 1,
                    heuristic_dist: 898 - x - y,
                },
                ExpandedPos {
                    x: 200 + x,
                    y,
                    d_num: 2,
                    heuristic_dist: 798 - x - y,
                },
                ExpandedPos {
                    x: 300 + x,
                    y,
                    d_num: 3,
                    heuristic_dist: 698 - x - y,
                },
                ExpandedPos {
                    x: 400 + x,
                    y,
                    d_num: 4,
                    heuristic_dist: 598 - x - y,
                },
                // Second row
                ExpandedPos {
                    x,
                    y: 100 + y,
                    d_num: 1,
                    heuristic_dist: 898 - x - y,
                },
                ExpandedPos {
                    x: 100 + x,
                    y: 100 + y,
                    d_num: 2,
                    heuristic_dist: 798 - x - y,
                },
                ExpandedPos {
                    x: 200 + x,
                    y: 100 + y,
                    d_num: 3,
                    heuristic_dist: 698 - x - y,
                },
                ExpandedPos {
                    x: 300 + x,
                    y: 100 + y,
                    d_num: 4,
                    heuristic_dist: 598 - x - y,
                },
                ExpandedPos {
                    x: 400 + x,
                    y: 100 + y,
                    d_num: 5,
                    heuristic_dist: 498 - x - y,
                },
                // Third row
                ExpandedPos {
                    x,
                    y: 200 + y,
                    d_num: 2,
                    heuristic_dist: 798 - x - y,
                },
                ExpandedPos {
                    x: 100 + x,
                    y: 200 + y,
                    d_num: 3,
                    heuristic_dist: 698 - x - y,
                },
                ExpandedPos {
                    x: 200 + x,
                    y: 200 + y,
                    d_num: 4,
                    heuristic_dist: 598 - x - y,
                },
                ExpandedPos {
                    x: 300 + x,
                    y: 200 + y,
                    d_num: 5,
                    heuristic_dist: 498 - x - y,
                },
                ExpandedPos {
                    x: 400 + x,
                    y: 200 + y,
                    d_num: 6,
                    heuristic_dist: 398 - x - y,
                },
                // Fourth row
                ExpandedPos {
                    x,
                    y: 300 + y,
                    d_num: 3,
                    heuristic_dist: 698 - x - y,
                },
                ExpandedPos {
                    x: 100 + x,
                    y: 300 + y,
                    d_num: 4,
                    heuristic_dist: 598 - x - y,
                },
                ExpandedPos {
                    x: 200 + x,
                    y: 300 + y,
                    d_num: 5,
                    heuristic_dist: 498 - x - y,
                },
                ExpandedPos {
                    x: 300 + x,
                    y: 300 + y,
                    d_num: 6,
                    heuristic_dist: 398 - x - y,
                },
                ExpandedPos {
                    x: 400 + x,
                    y: 300 + y,
                    d_num: 7,
                    heuristic_dist: 298 - x - y,
                },
                // Fifth row
                ExpandedPos {
                    x,
                    y: 400 + y,
                    d_num: 4,
                    heuristic_dist: 598 - x - y,
                },
                ExpandedPos {
                    x: 100 + x,
                    y: 400 + y,
                    d_num: 5,
                    heuristic_dist: 498 - x - y,
                },
                ExpandedPos {
                    x: 200 + x,
                    y: 400 + y,
                    d_num: 6,
                    heuristic_dist: 398 - x - y,
                },
                ExpandedPos {
                    x: 300 + x,
                    y: 400 + y,
                    d_num: 7,
                    heuristic_dist: 298 - x - y,
                },
                ExpandedPos {
                    x: 400 + x,
                    y: 400 + y,
                    d_num: 8,
                    heuristic_dist: 198 - x - y,
                },
            ];
            y += 1;
        }
        x += 1;
    }
    offsets
};

#[derive(Clone, Copy, Default)]
struct Case {
    /// Actual contents of the case.
    num: usize,
    /// Current shortest path from start to here.
    shortest_path_cost: Option<usize>,
    /// Estimated distance from here to end.
    heuristic_dist: usize,
}

impl fmt::Display for Case {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.num.fmt(f)
    }
}

fn a_star(grid: &mut Vec<Vec<Case>>) -> Result<()> {
    let size = grid.len();
    // We have 11 bins because the maximum fscore difference between the current node
    // and the nodes in the 'open' set is 10 (distance of 9 + heuristic diff of 1).
    // To that we need to add the case where there is equality.
    let max_cost = 9;
    let bin_nb = max_cost + 2;
    let mut bins = vec![vec!(); bin_nb];
    let mut cursor = 0;
    bins[cursor].push((0, 0));
    loop {
        // This loop assumes we will always have a next node to visit.
        // This is guaranteed by the A* algorithm as long as the end node is accessible.
        let (x, y) = loop {
            if let Some(next) = bins[cursor].pop() {
                break next;
            } else {
                cursor = (cursor + 1) % bin_nb;
            }
        };
        let curr = grid[x][y];
        if curr.heuristic_dist == 0 {
            // This is the final node!
            return Ok(());
        }
        for (neigh_x, neigh_y) in utils::around(x, y, size, false) {
            let neigh = &mut grid[neigh_x][neigh_y];
            let proposed_dist = neigh.num
                + curr.shortest_path_cost.ok_or_else(|| {
                    unreachable("Current node should have its cost calculated at this point")
                })?;
            if !matches!(neigh.shortest_path_cost,
                Some(cost) if proposed_dist >= cost)
            {
                // Found a better path than previously computed!
                neigh.shortest_path_cost = Some(proposed_dist);
                let fscore_diff = neigh.num + neigh.heuristic_dist - curr.heuristic_dist;
                // assert!(0 <= fscore_diff < bin_nb);
                let target_bin = (cursor + fscore_diff) % bin_nb;
                bins[target_bin].push((neigh_x, neigh_y));
            }
        }
    }
}

pub fn day(contents: String) -> Return {
    let size = contents
        .find('\n')
        .ok_or_else(|| Error::Parsing(contents.to_string()))?
        * 5;
    let mut grid = vec![vec![Case::default(); size]; size];
    grid[0][0].shortest_path_cost = Some(0);
    for (j, line) in contents.lines().enumerate() {
        for (i, ch) in line.chars().enumerate() {
            let d = ch.to_string().parse::<usize>()?;
            for ExpandedPos {
                x,
                y,
                d_num,
                heuristic_dist,
            } in EXPANSIONS[i][j]
            {
                let mut adjusted_num = d + d_num;
                if adjusted_num > 9 {
                    adjusted_num -= 9;
                }
                grid[x][y].num = adjusted_num;
                grid[x][y].heuristic_dist = heuristic_dist;
            }
        }
    }
    a_star(&mut grid)?;
    ret(grid[size - 1][size - 1]
        .shortest_path_cost
        .ok_or_else(|| unreachable("Final node should have its cost calculated at this point"))?)
}

#[cfg(test)]
mod day15 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 315);
        Ok(())
    }
}
