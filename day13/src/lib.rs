mod ocr;
use std::fmt;
use std::str::FromStr;
use utils::types::*;

enum FoldDirection {
    Horizontal,
    Vertical,
}

impl FromStr for FoldDirection {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        match s {
            "fold along y" => Ok(Self::Horizontal),
            "fold along x" => Ok(Self::Vertical),
            _ => Err(Error::Parsing(s.to_string())),
        }
    }
}

#[derive(Default)]
struct Paper {
    width: usize,
    height: usize,
    grid: Vec<Vec<bool>>,
}

impl fmt::Display for Paper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();
        for y in 0..self.height {
            for x in 0..self.width {
                if self.grid[x][y] {
                    s.push('█');
                } else {
                    s.push(' ');
                }
            }
            s.push('\n');
        }
        s.fmt(f)
    }
}

impl FromStr for Paper {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut paper = Self::default();
        for line in s.lines() {
            let (x, y) = line
                .split_once(',')
                .ok_or_else(|| Error::Parsing(line.to_string()))?;
            paper.add_mark(x.parse()?, y.parse()?);
        }
        Ok(paper)
    }
}

impl Paper {
    fn add_mark(&mut self, x: usize, y: usize) {
        if x >= self.width {
            self.grid.resize(x + 1, vec![false; self.height]);
            self.width = x + 1;
        }
        if y >= self.height {
            for column in self.grid.iter_mut() {
                column.resize(y + 1, false);
            }
            self.height = y + 1;
        }
        self.grid[x][y] = true;
    }

    fn fold(&mut self, side: FoldDirection, coord: usize) {
        match side {
            FoldDirection::Horizontal => {
                for column in self.grid.iter_mut() {
                    for i in 1..(self.height - coord) {
                        column[coord - i] |= column[coord + i];
                    }
                    column.truncate(coord);
                }
                self.height = coord;
            }
            FoldDirection::Vertical => {
                for i in 1..(self.width - coord) {
                    for y in 0..self.height {
                        self.grid[coord - i][y] |= self.grid[coord + i][y];
                    }
                }
                self.grid.truncate(coord);
                self.width = coord;
            }
        }
    }

    fn decode(self) -> Result<String> {
        let mut s = String::new();
        for chunk in self.grid.chunks(5) {
            let flat: Vec<_> = chunk.iter().flatten().collect();
            s.push(ocr::recog(&flat)?);
        }
        Ok(s)
    }
}

pub fn day(contents: String) -> Return {
    let mut fold_instructions: Vec<(FoldDirection, usize)> = Vec::new();
    let (coords, instructions) = contents
        .split_once("\n\n")
        .ok_or_else(|| Error::Parsing(contents.to_string()))?;
    let mut paper: Paper = coords.parse()?;
    for line in instructions.lines() {
        let (direction, num) = line
            .split_once('=')
            .ok_or_else(|| Error::Parsing(line.to_string()))?;
        fold_instructions.push((direction.parse()?, num.parse()?));
    }
    for (c, n) in fold_instructions {
        paper.fold(c, n);
    }
    ret(paper.decode()?)
}

#[cfg(test)]
mod day13 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, "HZKHFEJZ");
        Ok(())
    }
}
