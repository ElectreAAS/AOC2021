use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap};
use std::fmt;
use std::str::FromStr;
use utils::types::*;

#[derive(Clone, Copy, Hash, PartialEq)]
enum Amphipod {
    Amber,
    Bronze,
    Copper,
    Desert,
}

impl TryFrom<char> for Amphipod {
    type Error = Error;

    fn try_from(c: char) -> Result<Self> {
        Ok(match c {
            'A' => Self::Amber,
            'B' => Self::Bronze,
            'C' => Self::Copper,
            'D' => Self::Desert,
            _ => return Err(Error::Parsing(c.to_string())),
        })
    }
}

impl From<&Amphipod> for char {
    fn from(c: &Amphipod) -> Self {
        match c {
            Amphipod::Amber => 'A',
            Amphipod::Bronze => 'B',
            Amphipod::Copper => 'C',
            Amphipod::Desert => 'D',
        }
    }
}

impl Amphipod {
    fn home(&self) -> usize {
        match self {
            Self::Amber => 0,
            Self::Bronze => 1,
            Self::Copper => 2,
            Self::Desert => 3,
        }
    }

    fn cost(&self) -> usize {
        match self {
            Self::Amber => 1,
            Self::Bronze => 10,
            Self::Copper => 100,
            Self::Desert => 1000,
        }
    }
}

const FOLDED_LETTERS: [Amphipod; 8] = [
    Amphipod::Desert,
    Amphipod::Copper,
    Amphipod::Bronze,
    Amphipod::Amber,
    Amphipod::Desert,
    Amphipod::Bronze,
    Amphipod::Amber,
    Amphipod::Copper,
];

type Case = Option<Amphipod>;

const HALL_LENGTH: usize = 11;
const MOVE_TARGETS: [usize; 7] = [0, 1, 3, 5, 7, 9, 10];

#[derive(Clone, Default)]
struct Burrow {
    hallway: [Case; HALL_LENGTH],
    /// Rooms are ordered from left to right, and inside them from top to bottom.
    rooms: [[Case; 4]; 4],
    score: usize,
    heuristic: usize,
}

impl FromStr for Burrow {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut b = Burrow::default();
        let letters: Vec<Amphipod> = s.chars().filter_map(|c| c.try_into().ok()).collect();
        if letters.len() != 8 {
            return Err(Error::Parsing(s.to_string()));
        }
        for (i, letter) in letters.iter().enumerate() {
            b.rooms[i % 4][if i < 4 { 0 } else { 3 }] = Some(*letter);
        }
        for (i, letter) in FOLDED_LETTERS.iter().enumerate() {
            b.rooms[i % 4][if i < 4 { 1 } else { 2 }] = Some(*letter);
        }
        b.heuristic = b.start_heuristic();
        Ok(b)
    }
}

impl fmt::Display for Burrow {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // Ceiling of the burrow
        let mut s = String::from("█████████████\n█");
        // Hallway
        for c in self.hallway.iter() {
            match c {
                None => s.push(' '),
                Some(a) => s.push(a.into()),
            }
        }
        // Right of the hallway and left of the top rooms
        s.push_str("█\n███");
        // Top of the rooms
        for i in 0..4 {
            match &self.rooms[i][0] {
                None => s.push(' '),
                Some(a) => s.push(a.into()),
            }
            s.push('█');
        }
        // Right of the top rooms and left of the bot ones
        s.push_str("██\n  █");
        // Rest of the rooms
        for y in 1..4 {
            for x in 0..4 {
                match &self.rooms[x][y] {
                    None => s.push(' '),
                    Some(a) => s.push(a.into()),
                }
                s.push('█');
            }
            s.push_str("\n  █");
        }
        // Floor of the burrow
        s.push_str("████████");
        s.fmt(f)
    }
}

impl Burrow {
    fn is_room_valid(&self, room_nb: usize) -> bool {
        self.rooms[room_nb]
            .iter()
            .all(|case| !matches!(case, Some(a) if a.home() != room_nb))
    }

    /// Return the index at which an additionnal amphipod would insert themself.
    /// Return None when the room is either invalid or already full.
    fn room_target_index(&self, room_nb: usize) -> Option<usize> {
        let mut last_empty = None;
        for y in 0..4 {
            match &self.rooms[room_nb][y] {
                None => last_empty = Some(y),
                Some(a) if a.home() != room_nb => return None,
                _ => (),
            }
        }
        last_empty
    }

    fn is_winning(&self) -> bool {
        self.hallway.iter().all(|case| case.is_none())
            && (0..4).all(|room_nb| self.is_room_valid(room_nb))
    }

    /// Underestimating heuristic:
    /// We compute the cost of moving everyone into their place, **assuming they can overlap**.
    fn start_heuristic(&self) -> usize {
        // Cost of moving everyone down at the end.
        let mut sum = 11110;
        // Cost of moving everyone into the hallway and above their home.
        for (room_nb, room) in self.rooms.iter().enumerate() {
            for (y, case) in room.iter().enumerate() {
                match case {
                    Some(a) if a.home() != room_nb => {
                        sum += a.cost() * (y + 1 + 2 * utils::abs_diff(room_nb, a.home()))
                    }
                    // No need to move this one back down at the end, we save a bit.
                    Some(a) => sum -= (y + 1) * a.cost(),
                    _ => (),
                }
            }
        }
        sum
    }

    fn neighbours(&self) -> Vec<Self> {
        let mut v = Vec::new();
        // There are three kinds of possible moves:
        for (room_nb, room) in self.rooms.iter().enumerate() {
            for (y, case) in room.iter().enumerate() {
                match case {
                    Some(a) if (0..y).all(|n| self.rooms[room_nb][n].is_none()) => {
                        // First: someone moving directly from their initial room to their final room
                        match self.room_target_index(a.home()) {
                            Some(i) if room_nb != a.home() => {
                                let (min, max) =
                                    utils::min_max((room_nb + 1) * 2, (a.home() + 1) * 2);
                                if (min + 1..max).all(|n| self.hallway[n].is_none()) {
                                    let mut this = self.clone();
                                    this.rooms[a.home()][i] = Some(*a);
                                    this.rooms[room_nb][y] = None;
                                    let cost = a.cost() * (i + y + 2 + max - min);
                                    this.score += cost;
                                    this.heuristic -= cost;
                                    v.push(this);
                                }
                            }
                            None => {
                                // Second: someone moving from their initial room into the hallway
                                for target in MOVE_TARGETS {
                                    let (min, max) = utils::min_max(target, (room_nb + 1) * 2);
                                    if (min..=max).all(|n| self.hallway[n].is_none()) {
                                        let mut this = self.clone();
                                        this.hallway[target] = Some(*a);
                                        this.rooms[room_nb][y] = None;
                                        let cost = a.cost() * (y + 1 + max - min);
                                        this.score += cost;
                                        if room_nb == a.home() {
                                            this.heuristic += 2 * cost;
                                        } else {
                                            this.heuristic -= a.cost() * (y + 1);
                                            let dist_t_h =
                                                utils::abs_diff(target, (a.home() + 1) * 2);
                                            let dist_r_h = 2 * utils::abs_diff(room_nb, a.home());
                                            match dist_t_h.cmp(&dist_r_h) {
                                                // We got closer to the goal
                                                Ordering::Less => {
                                                    this.heuristic -=
                                                        a.cost() * (dist_r_h - dist_t_h)
                                                }
                                                // We moved away from it
                                                Ordering::Greater => {
                                                    this.heuristic +=
                                                        a.cost() * (dist_t_h - dist_r_h)
                                                }
                                                // No difference
                                                Ordering::Equal => (),
                                            }
                                        }
                                        v.push(this);
                                    }
                                }
                            }
                            _ => (),
                        }
                    }
                    _ => (),
                }
            }
        }
        // Finally: someone moving from the hallway into their room
        for (n, case) in self.hallway.iter().enumerate() {
            if let Some(a) = case {
                if let Some(i) = self.room_target_index(a.home()) {
                    let (min, max) = utils::min_max(n, (a.home() + 1) * 2);
                    if (min + 1..max).all(|n| self.hallway[n].is_none()) {
                        let mut this = self.clone();
                        this.rooms[a.home()][i] = Some(*a);
                        this.hallway[n] = None;
                        let cost = a.cost() * (i + 1 + max - min);
                        this.score += cost;
                        this.heuristic -= cost;
                        v.push(this);
                    }
                }
            }
        }

        v
    }
}

struct HeapBurrow(Burrow);

struct MapBurrow(Burrow);

mod type_indirections {
    use super::*;
    use std::hash::{Hash, Hasher};
    use std::ops::Deref;

    impl Deref for HeapBurrow {
        type Target = Burrow;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    impl Eq for HeapBurrow {}

    impl PartialEq for HeapBurrow {
        fn eq(&self, other: &Self) -> bool {
            self.score + self.heuristic == other.score + other.heuristic
        }
    }

    impl Ord for HeapBurrow {
        fn cmp(&self, other: &Self) -> Ordering {
            // Reversed!
            (other.score + other.heuristic).cmp(&(self.score + self.heuristic))
        }
    }

    impl PartialOrd for HeapBurrow {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Deref for MapBurrow {
        type Target = Burrow;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    impl Eq for MapBurrow {}

    impl PartialEq for MapBurrow {
        fn eq(&self, other: &Self) -> bool {
            self.hallway
                .iter()
                .zip(other.hallway.iter())
                .all(|(c1, c2)| c1 == c2)
                && self
                    .rooms
                    .iter()
                    .zip(other.rooms.iter())
                    .all(|(r1, r2)| r1.iter().zip(r2.iter()).all(|(c1, c2)| c1 == c2))
        }
    }

    impl Hash for MapBurrow {
        fn hash<H: Hasher>(&self, state: &mut H) {
            self.hallway.hash(state);
            self.rooms.hash(state);
        }
    }
}

fn a_star(start: Burrow) -> Result<usize> {
    let mut to_visit = BinaryHeap::new();
    to_visit.push(HeapBurrow(start));
    let mut shortest_paths = HashMap::new();
    while let Some(b) = to_visit.pop() {
        if b.is_winning() {
            return Ok(b.score);
        }

        for neigh in b.neighbours() {
            let prev_cost = shortest_paths
                .entry(MapBurrow(neigh.clone()))
                .or_insert(usize::MAX);
            if neigh.score < *prev_cost {
                *prev_cost = neigh.score;
                to_visit.push(HeapBurrow(neigh));
            }
        }
    }
    Err(unreachable(
        "No more states to visit but end state not reached!",
    ))
}

pub fn day(contents: String) -> Return {
    let b: Burrow = contents.parse()?;
    ret(a_star(b)?)
}

#[cfg(test)]
mod day23 {
    use super::*;

    #[test]
    fn test_input() -> Result<()> {
        let contents = utils::get_test()?;
        assert_eq!(day(contents)?, 44169);
        Ok(())
    }
}
